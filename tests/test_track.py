from aims3 import test_data_creator as tdc

import unittest
import numpy as np

class TestTrackFunctions(unittest.TestCase):

    def test_get_int_or_str(self):
        track = tdc.create_test_track()
        for k in ['Teff', 'L', 'R', 'numax', 'nuac', 'rho']:
            self.assertEqual(track[k][0], track[0][k])


    def test_get_slice(self):
        track = tdc.create_test_track()
        subtrack = track[:5]
        self.assertEqual(len(subtrack), 5)
        np.testing.assert_array_equal(subtrack.data, track.data[:5])
        np.testing.assert_array_equal(subtrack.idx, track.idx[:6])

        subtrack = track[2:-2]
        self.assertEqual(len(subtrack), len(track)-4)
        np.testing.assert_array_equal(subtrack.data, track.data[2:-2])
        np.testing.assert_array_equal(subtrack.idx, track.idx[2:-2]-track.idx[2])


    def test_get_list_of_ints(self):
        track = tdc.create_test_track()
        I = [0,3,9]
        subtrack = track[I]
        self.assertEqual(len(subtrack), len(I))
        np.testing.assert_array_equal(subtrack.data, track.data[I])

        idx = [track.idx[i+1]-track.idx[i] for i in I]
        np.testing.assert_array_equal(np.diff(subtrack.idx), idx)


    def test_get_list_of_strs(self):
        track = tdc.create_test_track()
        np.testing.assert_array_equal(track[['M','R']], [track.data['M'], track.data['R']])


    def test_get_list_of_tuples(self):
        track = tdc.create_test_track()
        ln = [(0,10),(1,5),(99,99)]
        modes = track[ln]
        np.testing.assert_array_equal(modes[0], track[ln[0]])
        self.assertTrue(np.all(np.isnan(modes[-1])))


    def test_get_errors(self):
        track = tdc.create_test_track()
        for key in ['asdfasdfasdf', ['asfads', (1,1)], 1.0, [1.0, 2.0]]:
            with self.assertRaises(KeyError):
                item = track[key]


    def test_setitem(self):
        track = tdc.create_test_track()

        track['M'] = 2
        np.testing.assert_allclose(track['M'], 2.0)

        track['M'] = 1.5
        np.testing.assert_allclose(track['M'], 1.5)

        track['FeH'] = np.log10(track['Zs']/track['Xs']/0.024884673998745162)
        np.testing.assert_allclose(track['FeH'], 0.06)
        np.testing.assert_allclose(track['FeH'][0], track[0]['FeH'])


    def test_append(self):
        ref = tdc.create_test_track()
        track = ref[:-1]
        track.append(ref[-1])
        np.testing.assert_array_equal(ref.data, track.data)
        np.testing.assert_array_equal(ref.modes, track.modes)
        np.testing.assert_array_equal(ref.idx, track.idx)


    def test_extend(self):
        ref = tdc.create_test_track()
        track = ref[:-3]
        track.extend(ref[-3:])
        np.testing.assert_array_equal(ref.data, track.data)
        np.testing.assert_array_equal(ref.modes, track.modes)
        np.testing.assert_array_equal(ref.idx, track.idx)


    def test_interpolate(self):
        for age_name in ['t', 'Xc']:
            track = tdc.create_test_track(N=50)
            model = track.interpolate(track[2][age_name]+1e-10, age_name=age_name)
            np.testing.assert_almost_equal(model['freq'], track[2]['freq'])

            model = track.interpolate(track[2][age_name]-1e-10, age_name=age_name)
            np.testing.assert_almost_equal(model['freq'], track[2]['freq'])

            model = track.interpolate((track[5][age_name]+track[6][age_name])/2, age_name=age_name)
            np.testing.assert_allclose(model['freq'], (track[5]['freq']+track[6]['freq'])/2,
                                       atol=1e-1)


    def test_interpolate_out_of_bounds(self):
        track = tdc.create_test_track(N=50)

        for t in [-np.inf, -1, track['t'].max()+1, np.inf]:
            with self.assertRaises(ValueError):
                model = track.interpolate(t)
