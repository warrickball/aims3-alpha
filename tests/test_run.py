from aims3 import test_data_creator as tdc
from aims3.model import Model
from aims3.stats import Likelihood

import unittest
import numpy as np

# try to mimic as complete a run as possible

class TestRun(unittest.TestCase):

    def test_run(self):
        Mrange = np.linspace(0.9, 1.10, 11)
        Zrange = 0.02*np.logspace(-0.3, 0.3, 7)
        grid = tdc.create_test_grid(Mrange=Mrange, Zrange=Zrange)

        grid.tessellate()

        obs = tdc.create_test_model()
        obs.modes = obs.modes[np.abs(obs.modes['freq']/obs['numax']-1) < 0.2]
        sigma = Model(np.array([('sigma', 1., 1.)],
                               dtype=[('name', 'U5'), ('L', float), ('R', float)]),
                      obs.modes.copy())
        sigma.modes['freq'] = 0.1
        like = Likelihood(obs, sigma)
        like.setup_frequency_function(function_names=['r01', 'r02'])

        like(grid.interpolate([1.0, 0.02, 4.5]))
