from aims3 import test_data_creator as tdc
from aims3.model import get_mode_loader

import os
import unittest
import numpy as np

data_dir = os.path.dirname(os.path.abspath(__file__)) + '/data/'
tmp = data_dir + 'tmp.grid'

eps = 1e-4
def jacobian_equals_finite_difference(model, name, free, fixed=None):
    dfreq, J = model.get_surface_correction(name, free, fixed=fixed)

    for i in range(len(free)):
        delta = free.copy()
        delta[i] = free[i]+eps
        up = model.get_surface_correction(name, delta, fixed=fixed)[0]
        delta[i] = free[i]-eps
        dn = model.get_surface_correction(name, delta, fixed=fixed)[0]
        np.testing.assert_allclose(J[i], (up-dn)/2/eps)


class TestModelFunctions(unittest.TestCase):

    def setUp(self):
        self.model = tdc.create_test_model()


    def test_mode_loaders(self):
        # TODO: recreate data correctly for AIMS formats, using GYRE output
        for mode_format in ['Simple', 'CLES', 'CLES_Mod', 'PLATO', 'GyRe', 'AGSM']:
            mode_loader = get_mode_loader(mode_format)
            modes = mode_loader(''.join([data_dir, 'modes.', mode_format.lower()]))

            np.testing.assert_array_equal(modes['l'], 1)
            np.testing.assert_array_equal(modes['n'], [19, 20, 21])

        # assert False
# AGSM l = 1, n = 19,20,21
# AGSM nu    [0.00283126 0.00296738 0.00310333]
# AGSM nu_Ri [0.00283096 0.00296704 0.00310294]

    def test_mul_int(self):
        model = tdc.create_test_model()*1
        np.testing.assert_array_equal(model.modes['freq'], self.model.modes['freq'])
        np.testing.assert_array_equal(model.modes['inertia'], self.model.modes['inertia'])


    def test_mul_float(self):
        model = tdc.create_test_model()*1.0
        np.testing.assert_array_equal(model.modes['freq'], self.model.modes['freq'])
        np.testing.assert_array_equal(model.modes['inertia'], self.model.modes['inertia'])


    def test_mul_error(self):
        for other in [self.model, 'asdfasdf']:
            with self.assertRaises(TypeError):
                error = self.model*other


    def test_add_model(self):
        model2 = 0.5*self.model + 0.5*self.model
        np.testing.assert_almost_equal(self.model['freq'], model2['freq'])


    def test_add_error(self):
        for other in [1.0, 1, 'asdfads', True]:
            with self.assertRaises(TypeError):
                error = self.model + other


    def test_get_special_key(self):
        model = tdc.create_test_model(M=1.0)
        self.assertLess(model['numax'], model['nuac'])
        model.data['R'] = 2.0
        self.assertEqual(model['rho'], 0.125)


    def test_get_slice(self):
        ref = tdc.create_test_model()
        model = ref[3:10:2]
        np.testing.assert_array_equal(ref.data, model.data)
        np.testing.assert_array_equal(ref.modes[3:10:2], model.modes)


    def test_get_list_of_ints(self):
        ref = tdc.create_test_model()
        I = [0,2,9]
        model = ref[I]
        np.testing.assert_array_equal(ref.data, model.data)
        np.testing.assert_array_equal(ref.modes[I], model.modes)


    def test_get_list_of_strs(self):
        model = tdc.create_test_model()
        np.testing.assert_array_equal(model[['R','M']], [model.data['R'], model.data['M']])


    def test_get_list_of_tuples(self):
        model = tdc.create_test_model()
        I = [10, len(model)//2, len(model)-20]
        l, n = [model.modes[k][I] for k in ['l', 'n']]
        modes = model[[*zip(l,n)]]
        for j, i in enumerate(I):
            np.testing.assert_array_equal(modes[j], model.modes[i]['freq'])
            np.testing.assert_array_equal(modes[j], model.modes[i]['freq'])


    def test_get_errors(self):
        model = tdc.create_test_model()
        for key in ['asdfasdfasdf', ['asfads', (1,1)], 1.0, [1.0, 2.0]]:
            with self.assertRaises(KeyError):
                item = model[key]


    def test_intersection(self):
        ref = tdc.create_test_model()
        model = ref.copy()
        model.modes = model.modes[::-1]
        I, J = ref.intersect_modes(model)
        np.testing.assert_array_equal(I, J[::-1])

        model = ref.copy()
        model.modes = model.modes[1::3]
        I, J = ref.intersect_modes(model)
        np.testing.assert_array_equal(ref.modes[I], model.modes[J])


    def test_set_n_None(self):
        ref = tdc.create_test_model()
        model = ref.copy()
        model.modes['n'] = -999
        model.set_n()
        np.testing.assert_array_equal(model.modes['n'], ref.modes['n'])

        # check that it works when there are only two consecutive radial modes
        ref.modes = ref.modes[[2] + list(range(3,len(ref.modes),3))]
        model = ref.copy()
        model.modes['n'] = -999
        model.set_n()
        np.testing.assert_array_equal(model.modes['n'], ref.modes['n'])


    def test_set_n_same(self):
        ref = tdc.create_test_model()
        model = ref.copy()
        model.modes['n'] = -999
        model.set_n(ref)
        np.testing.assert_array_equal(model.modes['n'], ref.modes['n'])


    def test_set_n_longer(self):
        ref = tdc.create_test_model()
        model = ref.copy()
        model.modes = model.modes[2::5]
        model.modes['n'] = -999
        model.set_n(ref)
        np.testing.assert_array_equal(model.modes['n'], ref.modes['n'][2::5])


    def test_setitem(self):
        model = tdc.create_test_model()

        model['M'] = 1
        self.assertAlmostEqual(model['M'], 1)

        model['M'] = 1.001
        self.assertAlmostEqual(model['M'], 1.001)

        model['FeH'] = np.log10(model['Zs']/model['Xs']/0.024884673998745162)
        self.assertAlmostEqual(model['FeH'], 0.06)


    # test implementation of surface corrections
    def test_bad_surface_correction_name(self):
        with self.assertRaises(ValueError):
            dfreq = self.model.get_surface_correction('asdf786b++1', [])


    def test_empty_surface_correction_name(self):
        np.testing.assert_array_equal(self.model.get_surface_correction('', [])[0], 0)


    def test_Ball2014(self):
        np.testing.assert_array_equal(self.model.get_surface_correction('Ball2014', [0])[0], 0)
        jacobian_equals_finite_difference(self.model, 'Ball2014', [1e-9])


    def test_Ball2014_2(self):
        np.testing.assert_array_equal(self.model.get_surface_correction('Ball2014_2', [0, 0])[0], 0)
        jacobian_equals_finite_difference(self.model, 'Ball2014_2', [1e-9,1e-10])


    def test_Sonoi2015(self):
        np.testing.assert_array_equal(self.model.get_surface_correction('Sonoi2015', [0], fixed=4.0)[0], 0)
        jacobian_equals_finite_difference(self.model, 'Sonoi2015', [1e-2], fixed=4.0)


    def test_Sonoi2015_scaling(self):
        np.testing.assert_array_equal(self.model.get_surface_correction('Sonoi2015_scaling', [0])[0], 0)
        jacobian_equals_finite_difference(self.model, 'Sonoi2015_scaling', [1e-2])


    def test_Sonoi2015_2(self):
        np.testing.assert_array_equal(self.model.get_surface_correction('Sonoi2015_2', [0, 1.7154])[0], 0)
        jacobian_equals_finite_difference(self.model, 'Sonoi2015_2', [1e-2,6])


    def test_Kjeldsen2008(self):
        np.testing.assert_array_equal(self.model.get_surface_correction('Kjeldsen2008', [0], fixed=4.9)[0], 0)
        jacobian_equals_finite_difference(self.model, 'Kjeldsen2008', [-5], fixed=4.9)


    def test_Kjeldsen2008_scaling(self):
        np.testing.assert_array_equal(self.model.get_surface_correction('Kjeldsen2008_scaling', [0])[0], 0)
        jacobian_equals_finite_difference(self.model, 'Kjeldsen2008_scaling', [-5])
        pass


    def test_PowerLaw(self):
        np.testing.assert_array_equal(self.model.get_surface_correction('PowerLaw', [0, 1.7154])[0], 0)
        jacobian_equals_finite_difference(self.model, 'PowerLaw', [-5,5])
