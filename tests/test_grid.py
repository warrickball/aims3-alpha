from aims3 import test_data_creator as tdc
from aims3.grid import load_binary_grid, load_text_grid

import os
import unittest
import numpy as np

data_dir = os.path.dirname(os.path.abspath(__file__)) + '/data/'
tmp = data_dir + 'tmp.grid'

class TestGridFunctions(unittest.TestCase):

    def test_len(self):
        Mrange = [0.95, 1.00, 1.05]
        Zrange = [0.015, 0.020, 0.025]
        grid = tdc.create_test_grid(Mrange=Mrange, Zrange=Zrange)
        self.assertEqual(len(grid), len(Mrange)*len(Zrange))


    def test_binary_round_trip(self):
        Mrange = [0.99, 1.01]
        Zrange = [0.015, 0.020, 0.025]
        grid = tdc.create_test_grid(Mrange=Mrange, Zrange=Zrange, metadata={'prefix': data_dir})
        grid.to_binary_file(tmp)
        grid2 = load_binary_grid(tmp)
        self.assertEqual(len(grid), len(grid2))


    def test_text_round_trip(self):
        # need to do something about all the model files that are created
        Mrange = [0.99,1.01]
        Zrange = [0.02]
        grid = tdc.create_test_grid(Mrange=Mrange, Zrange=Zrange, metadata={'prefix': data_dir})
        grid.to_text_file(tmp, save_models=True)
        grid2 = load_text_grid(tmp)


    def test_text_metadata_value_error(self):
        with self.assertRaises(ValueError):
            grid = load_text_grid(data_dir + 'test_metadata_err.grid')


    def test_get_int_or_str(self):
        Mrange = [1.00]
        Zrange = [0.02]
        grid = tdc.create_test_grid(Mrange=Mrange, Zrange=Zrange,
                                    metadata={'description': 'test_get_int_or_str'})
        track = tdc.create_test_track(Mrange[0], Zrange[0])
        model = tdc.create_test_model(Mrange[0], Zrange[0], t=0.0)

        for k in ['R', 'Teff', 'L']:
            self.assertEqual(grid[0][0][k], track[0][k])
            self.assertEqual(grid[0][0][k], model[k])

        self.assertEqual(grid['description'], 'test_get_int_or_str')


    def test_getitem_keyerror(self):
        grid = tdc.create_test_grid()
        for key in [1.0, [0, 'M'], 'asd__fa412sd!f~']:
            with self.assertRaises(KeyError):
                grid[key]


    def test_get_slice(self):
        Mrange = [0.99, 1.00, 1.01]
        Zrange = [0.019, 0.020, 0.021]
        grid = tdc.create_test_grid(Mrange=Mrange, Zrange=Zrange)
        subgrid = grid[::4]
        np.testing.assert_array_equal(subgrid.params, grid.params[::4])
        np.testing.assert_array_equal(subgrid[1][-1].data, grid[4][-1].data)
        np.testing.assert_array_equal(subgrid[1][-1].modes, grid[4][-1].modes)


    def test_get_list_of_ints(self):
        Mrange = [0.99, 1.00, 1.01]
        Zrange = [0.019, 0.020, 0.021]
        grid = tdc.create_test_grid(Mrange=Mrange, Zrange=Zrange)
        I = [0,1,4]
        subgrid = grid[I]
        self.assertEqual(len(subgrid), len(I))
        np.testing.assert_array_equal(subgrid.params, grid.params[I])
        np.testing.assert_array_equal(subgrid[1][-1].data, grid[I[1]][-1].data)
        np.testing.assert_array_equal(subgrid[1][-1].modes, grid[I[1]][-1].modes)


    def test_get_list_of_strs(self):
        Mrange = [0.99, 1.00, 1.01]
        Zrange = [0.019, 0.020, 0.021]
        grid = tdc.create_test_grid(Mrange=Mrange, Zrange=Zrange)
        np.testing.assert_array_equal(grid[grid.param_names], grid.params.T)


    def test_append(self):
        Mrange = [0.99, 1.00, 1.01]
        Zrange = [0.019, 0.020, 0.021]
        ref = tdc.create_test_grid(Mrange=Mrange, Zrange=Zrange)
        ref.refresh_params()

        grid = ref[:-1]
        grid.append(ref[-1])
        grid.refresh_params()

        np.testing.assert_array_equal(ref.params, grid.params)


    def test_extend(self):
        Mrange = [0.99, 1.00, 1.01]
        Zrange = [0.019, 0.020, 0.021]
        ref = tdc.create_test_grid(Mrange=Mrange, Zrange=Zrange)
        ref.refresh_params()

        grid = ref[:-3]
        grid.extend(ref[-3:])
        grid.refresh_params()

        np.testing.assert_array_equal(ref.params, grid.params)


    def test_interpolation_unscaled(self):
        grid = tdc.create_test_grid(Mrange=[0.99, 1.01], Zrange=[0.018, 0.022], N=50)
        grid.tessellate()
        model = grid.interpolate([1.0, 0.02, 4.5], scale=False)
        ref = tdc.create_test_model(1.0, 0.02, 4.5)
        _, I, J = np.intersect1d(ref.modes[['l','n']], model.modes[['l','n']],
                                 return_indices=True)
        np.testing.assert_almost_equal(ref['freq'][I], model['freq'][J], decimal=1)


    def test_interpolation_scaled(self):
        grid = tdc.create_test_grid(Mrange=[0.99, 1.01], Zrange=[0.018, 0.022], N=50)
        grid.tessellate()
        model = grid.interpolate([1.0, 0.02, 4.5], scale=True)
        ref = tdc.create_test_model(1.0, 0.02, 4.5)
        _, I, J = np.intersect1d(ref.modes[['l','n']], model.modes[['l','n']],
                                 return_indices=True)
        np.testing.assert_almost_equal(ref['freq'][I], model['freq'][J], decimal=0)


    def test_interpolation_out_of_bounds(self):
        grid = tdc.create_test_grid(Mrange=[0.99, 1.01], Zrange=[0.018, 0.022])
        grid.tessellate()

        for params in [[-1., 300., 1.]]:
            with self.assertRaises(ValueError):
                track = grid.interpolate(params)


    def test_interpolation_with_distortion_matrix(self):
        grid = tdc.create_test_grid(Mrange=np.linspace(0.95, 1.05, 6),
                                    Zrange=np.linspace(0.01, 0.03, 5))
        grid.tessellate()
        theta = np.pi/20
        R = np.array([[np.cos(theta), -np.sin(theta)],
                      [np.sin(theta),  np.cos(theta)]])
        ref = grid.interpolate([0.99, 0.019, 1])
        for i in range(10):
            grid.distortion_matrix = grid.distortion_matrix.dot(R)
            grid.tessellate()
            distort = grid.interpolate([0.99, 0.019, 1])
            for k in ['l', 'n', 'freq', 'inertia']:
                np.testing.assert_almost_equal(ref.modes[k], distort.modes[k])

        grid.distortion_matrix = np.array([[0., 1.], [1., 0.]])
        grid.tessellate()
        distort = grid.interpolate([0.99, 0.019, 1])
        for k in ['l', 'n', 'freq', 'inertia']:
            np.testing.assert_almost_equal(ref.modes[k], distort.modes[k])
