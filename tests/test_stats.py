from aims3 import test_data_creator as tdc
from aims3.stats import Likelihood, attr_list, unknown_n

import os
import unittest
import numpy as np

data_dir = os.path.dirname(os.path.abspath(__file__)) + '/data/'

class TestStatsFunctions(unittest.TestCase):

    # test that dnu0 gives differences between radial modes
    # test that you get nothing if not all l are available for r02,r01 or r10
    # check that nu gives identity for each l

    def setUp(self):
        self.obs = tdc.create_test_model(ls=[0,1,2,3])
        self.sigma = tdc.create_test_model(ls=[0,1,2,3])
        I = np.abs(self.obs['freq']/self.obs['freq'].max()-0.6) < 0.2
        self.sigma.modes = self.sigma.modes[I]
        self.obs.modes = self.obs.modes[I]
        self.sigma.data = np.array(('sigma', 0.01, 0.01),
                                   dtype=[('name', 'U5'), ('L', float), ('R', float)])
        self.sigma.modes['freq'] = (np.arange(len(self.sigma.modes)) + 0.1)%0.4
        self.likelihood = Likelihood(self.obs, self.sigma)


    def test_singular_covariance(self):
        with self.assertRaises(np.linalg.LinAlgError):
            self.likelihood.setup_frequency_function(function_names=['dnu','dnu'])


    def test_call(self):
        self.likelihood.setup_frequency_function(function_names=['r01','r02'])
        other = self.obs.copy()
        np.testing.assert_almost_equal(self.likelihood(other), 0)

        self.likelihood.surface_correction_name = 'Ball2014'
        np.testing.assert_almost_equal(self.likelihood(other), 0)

        np.testing.assert_almost_equal(self.likelihood(other, a=[0]), 0)

        
    def test_frequency_function_identity(self):
        self.likelihood.setup_frequency_function(function_names=['nu'])
        np.testing.assert_almost_equal(self.likelihood.freq_func_num, np.eye(len(self.obs)))


    def test_frequency_function(self):
        for function_name in ['nu', 'nu0', 'nu1', 'nu2', 'nu3', 'r02', 'r13', 'r01', 'r10',
                              'dnu', 'd2nu', 'avg_dnu']:
            self.likelihood.setup_frequency_function(function_names=[function_name])
            np.testing.assert_almost_equal(self.likelihood.frequency_function(self.obs['freq']),
                                           self.likelihood.freq_func_obs)


    def test_empty_frequency_function(self):
        I = self.likelihood.obs.modes['l'] == 0
        self.likelihood.obs.modes = self.likelihood.obs.modes[I]
        self.likelihood.sigma.modes = self.likelihood.sigma.modes[I]
        self.likelihood.setup_frequency_function(function_names=['r01','r10','r02'])

        self.assertEqual(len(self.likelihood.freq_func_num), 0)
        self.assertEqual(len(self.likelihood.freq_func_den), 0)


    def test_dnu_frequency_function(self):
        dnu = np.diff(self.obs.modes['freq'][:2])[0] # same for all (l,n) in current test data
        
        self.likelihood.setup_frequency_function(function_names=['dnu'])
        np.testing.assert_almost_equal(self.likelihood.freq_func_obs, dnu)

        for i in range(4):
            self.likelihood.setup_frequency_function(function_names=['dnu%i' % i])
            np.testing.assert_almost_equal(self.likelihood.freq_func_obs, dnu)


    def test_avg_dnu_frequency_function(self):
        dnu = np.diff(self.obs.modes['freq'][:2])[0] # same for all (l,n) in current test data

        self.likelihood.setup_frequency_function(function_names=['avg_dnu'])
        np.testing.assert_almost_equal(self.likelihood.freq_func_obs, dnu)

        for i in range(4):
            self.likelihood.setup_frequency_function(function_names=['avg_dnu%i' % i])
            np.testing.assert_almost_equal(self.likelihood.freq_func_obs, dnu)


    def test_nu_min_frequency_function(self):
        obs_radial = np.sort(self.obs['freq'][self.obs['l']==0])
        self.likelihood.setup_frequency_function(function_names=['nu_min'])
        np.testing.assert_almost_equal(self.likelihood.freq_func_obs, obs_radial[0])

        for i in range(3):
            self.likelihood.setup_frequency_function(function_names=['nu_min%i' % i])
            np.testing.assert_almost_equal(self.likelihood.freq_func_obs, obs_radial[i])


    def test_set_chi2_weights(self):

        # nonseismic measurements are L and R => n_nonseismic = 2
        # add 'avg_dnu' for l = 0, 1 and 2 as seismic constraint so n_seismic = 3
        self.likelihood.setup_frequency_function(function_names=['avg_dnu0', 'avg_dnu1', 'avg_dnu2'])

        # reference values: {weight option: (seismic, non_seismic)}
        ref = {None: (1.0, 1.0),
               'absolute': (1.0, 1.0),
               'relative': (2.0/3.0, 1.0),
               'reduced': (0.25, 0.25),
               'reduced_bis': (0.5, 1.0)}

        for key, (seismic_weight, nonseismic_weight) in ref.items():
            self.likelihood.set_chi2_weights(key)
            self.assertEqual(self.likelihood.seismic_weight, seismic_weight)
            self.assertEqual(self.likelihood.nonseismic_weight, nonseismic_weight)


    def test_load_observations(self):
        self.likelihood.load_observations(data_dir + 'test.aims3obs')

        self.assertEqual(self.likelihood.obs['Teff'], 5761.1636917918304)
        self.assertEqual(self.likelihood.sigma['Teff'], 84.0)

        self.assertEqual(self.likelihood.obs['M'], 1.0)
        self.assertEqual(self.likelihood.sigma['M'], 0.001)
        
        self.assertEqual(self.likelihood.obs['Z'], 0.02)
        self.assertEqual(self.likelihood.sigma['Z'], 0.0001)

        self.assertEqual(self.likelihood.obs.modes['n'][0], 18)
        self.assertEqual(self.likelihood.obs.modes['freq'][0], 2467.298205749250)
        self.assertEqual(self.likelihood.sigma.modes['freq'][0], 1.10)

        self.assertEqual(self.likelihood.obs.modes['freq'][-1], 3531.704217299360)
        self.assertEqual(self.likelihood.sigma.modes['freq'][-1], 1.30)
        self.assertEqual(self.likelihood.obs.modes['n'][-1], unknown_n)


    def test_observations_round_trip(self):
        obs_file = data_dir + 'tmp.aims3obs'
        self.likelihood.save_observations(obs_file, float_fmt='%17.10f')
        other = Likelihood(None, None)
        other.load_observations(obs_file)

        # for what in what?
        #     self.assertEqual(self.likelihood.obs, other.obs)
        #     self.assertEqual(self.likelihood.sigma, other.sigma)
        for k in self.sigma.data.dtype.names:
            if k != 'name':
                np.testing.assert_almost_equal(self.likelihood.obs.data[k],
                                               other.obs.data[k])
                np.testing.assert_almost_equal(self.likelihood.sigma.data[k],
                                               other.sigma.data[k])
        
        for k in ['l','n','freq']:
            np.testing.assert_almost_equal(self.likelihood.obs.modes[k], other.obs.modes[k])


    # for each, test that we recover the parameters we add
    def test_optimal_surface_correction(self):
        test_free = {'PowerLaw': [-3., 4.],
                     'Kjeldsen2008': [-3.],
                     'Kjeldsen2008_scaling': [-3.],
                     'Ball2014': [-1e-10],
                     'Ball2014_2': [-1e-10, -1e-11],
                     'Sonoi2015': [-3.],
                     'Sonoi2015_scaling': [-3.],
                     'Sonoi2015_2': [-3., 4.]}
        fixed = 4.0

        model = tdc.create_test_model()
        I, J = model.intersect_modes(self.obs)
        model.modes = model.modes[I]

        for name in ['PowerLaw', 'Kjeldsen2008', 'Kjeldsen2008_scaling',
                     'Ball2014', 'Ball2014_2', 'Sonoi2015', 'Sonoi2015_2',
                     'Sonoi2015_scaling']:
            obs = self.obs.modes['freq'].copy()
            free = self.likelihood.get_optimal_surface_correction(model, name=name, obs=obs, fixed=fixed)
            self.assertAlmostEqual(free[0], 0)

            obs += model.get_surface_correction(name, test_free[name], fixed=fixed)[0]
            free = self.likelihood.get_optimal_surface_correction(model, name=name, obs=obs, fixed=fixed)
            np.testing.assert_allclose(test_free[name], free)
            np.testing.assert_allclose(obs, model.modes['freq'] + model.get_surface_correction(name, free, fixed)[0])


    def test_bad_surface_correction_name(self):
        model = tdc.create_test_model()

        # default name is '', which is invalid
        with self.assertRaises(ValueError):
            free = self.likelihood.get_optimal_surface_correction(model)

        with self.assertRaises(ValueError):
            free = self.likelihood.get_optimal_surface_correction(model, name='asdfasdfasdfa')


    def test_surface_correction_cross_checks(self):
        model = tdc.create_test_model()
        I, J = model.intersect_modes(self.obs)
        model.modes = model.modes[I]
        obs = self.obs.modes['freq'].copy()

        # BG14-2 is the same as BG14-1 with a_{-1} = 0
        dfreq = model.get_surface_correction('Ball2014', [-1e-10])[0]
        free = self.likelihood.get_optimal_surface_correction(model, name='Ball2014_2', obs=obs+dfreq)
        self.assertAlmostEqual(free[1], 0)

        # Kjeldsen2008 is the same as Kjeldsen2008_2 with free[1] = fixed[0]
        dfreq = model.get_surface_correction('Kjeldsen2008', [-3.], fixed=[5.])[0]
        free = self.likelihood.get_optimal_surface_correction(model, name='Kjeldsen2008_2', obs=obs+dfreq)
        self.assertAlmostEqual(free[1], 5.)

        # Kjeldsen2008_scaling is the same as Kjeldsen2008_2 with free[1] = scaling value
        dfreq = model.get_surface_correction('Kjeldsen2008_scaling', [-3.])[0]
        free = self.likelihood.get_optimal_surface_correction(model, name='Kjeldsen2008_2', obs=obs+dfreq)
        self.assertAlmostEqual(free[1], model.b_Kjeldsen2008)

        # Sonoi2015 is the same as Sonoi2015_2 with free[1] = fixed[0]
        dfreq = model.get_surface_correction('Sonoi2015', [-3.], fixed=[5.])[0]
        free = self.likelihood.get_optimal_surface_correction(model, name='Sonoi2015_2', obs=obs+dfreq)
        self.assertAlmostEqual(free[1], 5.)

        # Sonoi2015_scaling is the same as Sonoi2015_2 with free[1] = scaling value
        dfreq = model.get_surface_correction('Sonoi2015_scaling', [-3.])[0]
        free = self.likelihood.get_optimal_surface_correction(model, name='Sonoi2015_2', obs=obs+dfreq)
        self.assertAlmostEqual(free[1], model.beta_Sonoi2015)


    # this has become part of CLI
    # TODO: move config tests to CLI

    # def test_load_config(self):
    #     self.likelihood.load_config(data_dir + 'test.aims3cfg')

    #     self.assertEqual(self.likelihood.seismic_weight, 2.0)
    #     self.assertEqual(self.likelihood.nonseismic_weight, 3.0)
    #     self.assertEqual(self.likelihood.surface_correction_name, None)

        
    # def test_config_round_trip(self):
    #     config_file = data_dir + 'tmp.aims3cfg'
    #     self.likelihood.save_config(config_file)
    #     other = Likelihood(None, None)
    #     other.load_config(config_file)

    #     for k in attr_list:
    #         self.assertEqual(getattr(self.likelihood, k), getattr(other, k))
