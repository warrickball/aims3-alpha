from argparse import ArgumentParser
import numpy as np
import yaml
from aims3.grid import load_binary_grid

def get_parser():
    parser = ArgumentParser()
    subparsers = parser.add_subparsers(metavar='command')


    # compile a text grid into binary format
    compile_grid_parser = subparsers.add_parser(
        'compile-grid',
        help="compile grid")
    compile_grid_parser.add_argument('input_filename', type=str, nargs='?')
    compile_grid_parser.add_argument('output_filename', type=str, nargs='?')
    compile_grid_parser.add_argument('--protocol', type=int, default=4,
                                     help="pickle protocol (default=4)")
    compile_grid_parser.add_argument('-F', '--mode-format', type=str,
                                     help="format for mode files (default='simple')",
                                     default='simple')
    compile_grid_parser.add_argument('-v', '--verbose', action='store_true',
                                     help="show progress")
    compile_grid_parser.set_defaults(func=compile_grid)


    # print info about a binary grid
    grid_info_parser = subparsers.add_parser(
        'grid-info',
        help="show information about grid")
    grid_info_parser.add_argument('grid_filename', type=str)
    grid_info_parser.set_defaults(func=grid_info)
    
    # TODO: CLI test grid interpolation
    run_interp_test_parser = subparsers.add_parser(
        'run-interp-test',
        help="test interpolation")
    run_interp_test_parser.add_argument('grid_filename', type=str)
    run_interp_test_parser.add_argument('-v', '--verbose', action='store_true')
    run_interp_test_parser.add_argument('--track-increments', type=str, nargs='+',
                                        default=[1])
    run_interp_test_parser.add_argument('--mode', type=str, default='relative',
                                        choices=['relative', 'absolute'])
    run_interp_test_parser.set_defaults(func=run_interp_test)

    # TODO: CLI plot grid interpolation
    analyse_interp_test_parser = subparsers.add_parser(
        'analyse-interp-test',
        help="test interpolation")
    analyse_interp_test_parser.set_defaults(func=analyse_interp_test)

    # run MCMC
    run_mcmc_parser = subparsers.add_parser(
        'run-mcmc',
        help="run MCMC analysis")
    run_mcmc_parser.add_argument('config_files', type=str, nargs='+')
    run_mcmc_parser.set_defaults(func=run_mcmc)
    # TODO: set some CLI MCMC options from command line


    # analyse results of a previous MCMC run
    run_mcmc_parser = subparsers.add_parser(
        'analyse-mcmc',
        help="analyse previous MCMC run")
    run_mcmc_parser.add_argument('config_files', type=str, nargs='+')
    run_mcmc_parser.set_defaults(func=analyse_mcmc)

    return parser

# TODO: split this up into files more neatly
# TODO: test CLI routines

def compile_grid(args):
    from aims3.grid import load_text_grid
    from aims3.model import get_mode_loader

    mode_loader = get_mode_loader(args.mode_format.lower())
    grid = load_text_grid(args.input_filename,
                          mode_loader=mode_loader,
                          progress=args.verbose)
    grid.to_binary_file(args.output_filename, protocol=args.protocol)


def run_interp_test(args):
    import pickle

    def vprint(*print_args, **print_kwargs):
        if args.verbose:
            print(*print_args, **print_kwargs)

    grid = load_binary_grid(args.grid_filename)

    # err is a list of lists of arrays
    # err[0] is grid error
    # err[k] is track error for each choice of increment
    # err[0][i][j] is the max rel freq error in grid[i][j]
    err = [[np.full(len(track), np.nan) for track in grid] for i in range(1+len(args.track_increments))]

    if args.mode == 'relative':
        def diff(a, b):
            return np.nanmax(np.abs(a/b-1))
    elif args.mode == 'absolute':
        def diff(a, b):
            return np.nanmax(np.abs(a-b))
    else:
        raise ValueError("args.mode must be 'relative' or 'absolute', not '%s'" % args.mode)

    # I doubt these for loops contribute much to the runtime, so separate them because it's more logical
    
    # test interpolation along each track with 1 and 2 steps in age
    for i, track in enumerate(grid):
        vprint('\rTesting track interpolation: track %i of %i (%i%%)... ' % (i+1, len(grid), (i+1)/len(grid)*100), end='')
        for j, model in enumerate(track):
            for n, delta_j in enumerate(args.track_increments):
                if j < delta_j or j > len(track)-delta_j-1:
                    continue

                indices = [k for k in range(len(track)) if delta_j <= abs(k-j)]
                interp = track[indices].interpolate(model['t'])
                I, J = model.intersect_modes(interp)
                err[n+1][i][j] = diff(interp.modes['freq'][J], model.modes['freq'][I])

    vprint('Done.')

    # test interpolation in grid by omitting points
    
    for i in range(len(grid)):
        vprint('\rTesting grid  interpolation: track %i of %i (%i%%)... ' % (i+1, len(grid), (i+1)/len(grid)*100), end='')

        indices = [j for j in range(len(grid)) if not j == i]
        subgrid = grid[indices]
        subgrid.tessellate()
        for j, model in enumerate(grid[i]):

            params = np.hstack([grid.params[i], model['t']])

            try:
                interp = subgrid.interpolate(params)
                I, J = model.intersect_modes(interp)
                err[0][i][j] = diff(interp.modes['freq'][J], model.modes['freq'][I])
            except ValueError:
                continue

        del subgrid # very defensive: omitting this line doesn't seem to increase memory usage

    vprint('Done.')

    with open('err.pk', 'wb') as f:
        pickle.dump(err, f, protocol=None)


def analyse_interp_test(args):
    pass


def grid_info(args):
    # load grid
    print('\nfilename: %s' % args.grid_filename)
    print(load_binary_grid(args.grid_filename))


# default config for all mcmc methods
def default_mcmc_config():
    return {'output_folder': 'results/__obs_filename__'}


def run_mcmc(args):
    import scipy.stats as spstats
    import emcee

    from aims3.stats import Likelihood

    config = default_mcmc_config()
    for filename in args.config_files:
        with open(filename, 'r') as f:
            config = {**config, **yaml.safe_load(f)}

    output_folder = config['output_folder'].replace('__obs_filename__', config['obs_filename'])
    # TODO: "no overwrite" option, needs to force exit here

    # load grid
    grid = load_binary_grid(config['grid_filename'])
    grid.tessellate()

    # setup likelihood
    like = Likelihood(None, None)
    like.load_observations(config['obs_filename'])
    like.setup_frequency_function(config['seismic_constraints'])
    like.set_chi2_weights(config['weight_option'],
                          seismic_weight_factor=config['seismic_weight'],
                          nonseismic_weight_factor=config['nonseismic_weight'])

    like.surface_correction_name = config['surface_correction_name']

    if config['surface_correction_name'] == 'Ball2014_2':
        surface_correction_param_names = ['a_3', 'a_-1']
    
    param_names = grid.param_names + ['t'] + surface_correction_param_names
    
    # setup priors
    priors = []
    for param in param_names:
        if param in config['priors'].keys():
            kind = config['priors'][param]['kind']
            prior_params = config['priors'][param]['params']
            if kind.lower() == 'uniform':
                priors.append(spstats.uniform(
                    loc=prior_params[0], scale=prior_params[1]-prior_params[0]))
            # TODO: implement prior distributions
            else:
                raise ValueError("unrecognized kind of prior distribution '%s' " % kind +
                                 "for parameter '%s'" % param)
        else:
            if param in grid.param_names:
                i = grid.param_names.index(param)
                param_min = grid.params[:,i].min()
                param_max = grid.params[:,i].max()
                priors.append(spstats.uniform(
                    loc=param_min, scale=param_max-param_min))
            else:
                priors.append(None)

    # setup sampler (TODO: implement options to choose sampler)
    D_grid = len(grid.param_names)
    D_surf = len(surface_correction_param_names)

    def log_prior(x):
        return sum([prior.logpdf(xi) for (xi, prior) in zip(x, priors) if prior is not None])

    def log_posterior(x):
        try:
            return like(grid.interpolate(x[:-D_surf]), a=x[-D_surf:]) + log_prior(x)
        except:
            return -np.inf

    W = config['nwalkers']
    D = len(param_names)

    S = emcee.EnsembleSampler(W, D, log_posterior)

    # use "tight ball" around best
    if config['init_walkers'].lower() == 'tight_ball':
        best = None
        prob = -np.inf
        for track in grid:
            for model in track:
                like_model = like(model)
                if like_model > prob:
                    best = model.copy()
                    prob = like_model + 0

        match = best.copy()
        match.modes = match.modes[match.intersect_modes(like.obs)[0]]
        p0 = np.zeros(D)
        p0[:-D_surf] = [best.data[k] for k in param_names[:-D_surf]]
        p0[-D_surf:] = like.get_optimal_surface_correction(match)

        # TODO: refactor all this; this should be reflected in priors
        p_min, p_max = np.zeros((2,D))
        p_min[:D_grid] = grid.params.min(axis=0)
        p_max[:D_grid] = grid.params.max(axis=0)
        p_max[D_grid] = np.min([track['t'][-1] for track in grid])
        p_max[-D_surf:] = p0[-D_surf:]

        dp0 = np.random.randn(W,D)
        dp0 /= (np.sum(dp0**2, axis=1)**0.5)[:,None]
        pos = p0 + (p_max-p_min)*dp0/30
        
    # sample from priors
    elif config['init_walkers'].lower() == 'priors':
        pos = np.full((W,D), np.nan)
        pos[:,:-D_surf] = np.vstack([prior.rvs(size=W) for prior in priors[:-D_surf]]).T
        for i in range(D-D_surf, D):
            if priors[i] is None:
                for j, pj in enumerate(pos[:,:-D_surf]):
                    try:
                        match = grid.interpolate(pj)
                        I, J = match.intersect_modes(like.obs)
                        match.modes = match.modes[I]
                        pos[j,-D_surf:] = like.get_optimal_surface_correction(match)
                    except:
                        continue
                
                bad = ~np.isfinite(pos[:,-1])
                pos[bad,-D_surf:] = np.random.uniform(low=np.min(pos[~bad,-D_surf:], axis=0),
                                                     high=np.max(pos[~bad,-D_surf:], axis=0),
                                                     size=(sum(bad),D_surf))
                break
            else:
                pos[:,i] = priors[i].rvs(size=W)

    else:
        raise ValueError("invalid option '%s' for config option 'init_walkers'"
                         % (config['init_walkers']))

    if config['include_best']:
        # TODO: implement 'include_best'
        pass

    pos, prob, state = S.run_mcmc(pos, config['nsteps0'])
    S.reset()
    pos, prob, state = S.run_mcmc(pos, config['nsteps'])

    # create output folder if it doesn't exist

    # by default, at least dump the MCMC chain and lnprob
    np.save('/'.join([output_folder, config['obs_filename']+'.mcmc_dump.npy']),
            np.dstack((S.chain, S.lnprobability)))


def analyse_mcmc(args):
    config = default_mcmc_config()
    for filename in args.config_files:
        with open(filename, 'r') as f:
            config = {**config, **yaml.safe_load(f)}

    output_folder = config['output_folder'].replace('__obs_filename__', config['obs_filename'])

    # load grid
    grid = load_binary_grid(config['grid_filename'])
    grid.tessellate()

    # load MCMC results
    mcmc_dump = np.load('/'.join([output_folder, config['obs_filename']+'.mcmc_dump.npy']))

    # TODO: mcmc analyser to read a matplotlib style file

    # these should be views and therefore not take any more memory
    chain = mcmc_dump[:,:,:-1]
    lnprob = mcmc_dump[:,:,-1]

    # just do everything for now, give it names later

    # corner plot
    from corner import corner
    fig = corner(chain.reshape((-1,chain.shape[-1])))
    fig.savefig('/'.join([output_folder, 'corner_params.png']))
    
    # TODO: implement big corner plot
    # TODO: decide what other output is necessary or helpful
