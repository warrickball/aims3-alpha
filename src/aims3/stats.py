import numpy as np
import yaml

from aims3.model import Model, mode_dtype

# list of valid attributes for Likelihood class
attr_list = ['seismic_weight', 'nonseismic_weight', 'surface_correction_name']
unknown_n = 999

# mode_dtype = [('l', np.uint8), ('n', np.int16),
#               ('freq', np.float64), ('sigma', np.float64)]

class Likelihood:
    """An object to setup and evaluate a likelihood function for fitting
    models to observations.

    Calling the object evaluates the likelihood, i.e. –χ²/2.

    """
    def __init__(self, obs, sigma):
        # TODO: implement data functions
        # TODO: stop using Models for obs and sigma
        # if we don't know n, how do we guess?
        # how to handle other constraints? put in obs but not sigma?
        if obs is None:
            self.obs = None
        else:
            self.obs = obs.copy()
            self.obs.data['name'] = 'Likelihood.obs'

        if sigma is None:
            self.sigma = None
        else:
            self.sigma = sigma.copy()
            self.sigma.data['name'] = 'Likelihood.sigma'

        # data for frequency function
        self.freq_func_names = []
        self.freq_func_num = None
        self.freq_func_den = None
        self.freq_func_is_ratio = None
        self.freq_func_obs = None
        self.freq_func_cov = None
        self.freq_func_icov = None

        self.seismic_weight = 1.0
        self.nonseismic_weight = 1.0
        self.surface_correction_name = ''


    def __call__(self, model, a=None):
        return -0.5*self.chi2(model, a=a)


    def chi2(self, model, a=None):
        """Return χ² of the given ``model`` using surface correction
        parameters ``a``."""
        return self.nonseismic_weight*self.chi2_nonseismic(model) \
            + self.seismic_weight*self.chi2_seismic(model, a=a)


    def chi2_seismic(self, model, a=None):
        """Return the seismic χ² of the given ``model`` using surface
        correction parameters ``a``."""
        # match modes
        # TODO: clean up mode matching in chi2_seismic
        I_model, I_obs = model.intersect_modes(self.obs)
        matched = model.copy()
        matched.modes = matched.modes[I_model]

        # apply frequency correction
        if self.surface_correction_name == "Roxburgh2016":
            # TODO: implement phase matching
            # chi2_seismic = blah
            pass
        else:
            if self.surface_correction_name is None or self.surface_correction_name == '':
                # no surface correction
                corr = matched['freq']
            else:
                if a is None:
                    # optimize surface correction parameters
                    a = self.get_optimal_surface_correction(matched)

                # apply (maybe optimised) surface correction parameters
                corr = matched['freq'] + matched.get_surface_correction(self.surface_correction_name, a)[0]

            # χ²(seismic) = [f(matched)-f(obs)]' C¯¹ [f(matched)-f(obs)]
            diff = self.frequency_function(corr)-self.freq_func_obs
            return diff.dot(self.freq_func_icov).dot(diff)

    def chi2_nonseismic(self, model):
        """Return the non-seismic χ² of the given ``model``."""
        return np.sum([((model.data[k] - self.obs.data[k])/self.sigma.data[k])**2
                       for k in self.sigma.data.dtype.names if k != 'name'])


    # want a (maybe non-linear) function f(ν) such that
    #    χ²(seismic) = [f(corr)-f(obs)]C¯¹[f(corr)-f(obs)]
    # each element of f is either a linear map or a ratio
    # so define A and B s.t. f(ν) = Aν./Bν (i.e. element-wise division)

    def extend_frequency_function(self, names, num_rows, den_rows=None):
        """A utility function to extend the frequency function by appending
        the ``names`` of the new components, their numerator matrix
        ``num_rows`` and, if present, denominator matrix ``den_rows``."""

        self.freq_func_names.extend([names])
        self.freq_func_num = np.vstack([self.freq_func_num, num_rows])
        if den_rows is None:
            self.freq_func_den = np.vstack([self.freq_func_den, 0.*num_rows])
        else:
            self.freq_func_den = np.vstack([self.freq_func_den, den_rows])


    # combinations are specified by a list of [Δl,Δn,coeff] for each of numerator and denominator
    def setup_frequency_function(self, function_names=[]):
        """Set up the frequency function using the list of options in
        ``function_names`` (e.g. ``['avg_dnu1', 'r02']``).  Current
        options (see :ref:`freqfunc`) are:

        * ``nu<l>``: the individual frequencies
        
        * ``nu_min<i>``: use the *i*-th lowest radial mode frequency
        
        * ``r02``: the r₀₂ separation ratios

        * ``r13``: the r₁₃ separation ratios

        * ``r01``: the r₀₁ separation ratios

        * ``r10``: the r₁₀ separation ratios

        * ``d2nu``: the second frequency differences 
          :math:`\\Delta_2\\nu_{l,n}=\\nu_{l,n-1}-2\\nu_{l,n}+\\nu_{l,n+1}`

        * ``dnu<l>``: the first frequency differences
          :math:`\\Delta\\nu_{l,n}=\\nu_{l,n}-\\nu_{l,n-1}`

        * ``avg_dnu<l>``: the average large separation
          :math:`\\langle\\Delta\\nu_l\\rangle` (found by linear
          regression)

        ``<l>`` indicates an optional angular degree.  If omitted, all
        available angular degrees will be used.  ``avg_dnu`` gives a
        single large separation fit by regression using all the
        modes.

        """

        self.freq_func_names = []
        self.freq_func_num = np.zeros((0, len(self.obs.modes)))
        self.freq_func_den = self.freq_func_num + 0

        for name in function_names:
            if name.startswith('nu_min'):
                try:
                    k = int(name[6:])
                except ValueError:
                    k = 0

                i = np.argsort(self.obs.modes['freq'][self.obs.modes['l']==0])[k]
                num_row = np.zeros(len(self.obs.modes))
                num_row[np.where(self.obs.modes['l']==0)[0][i]] = 1.0
                self.extend_frequency_function([name], num_row)

            elif name == 'nu':
                self.setup_ratios(name, num=[(0,0,1.0)])

            elif name.startswith('nu'):
                self.setup_ratios(name, num=[(0,0,1.0)], l_ref=[int(name[2:])])

            elif name == 'r02':
                self.setup_ratios(name, num=[(0,0,1.0),(2,-1,-1.0)], den=[(1,0,1.0),(1,-1,-1.0)], l_ref=[0])

            elif name == 'r13':
                self.setup_ratios(name, num=[(0,0,1.0),(2,-1,-1.0)], den=[(-1,0,1.0),(-1,-1,-1.0)], l_ref=[1])

            elif name == 'r01':
                self.setup_ratios(name, num=[(0,-1,0.125),(1,-1,-0.5),(0,0,0.75),(1,0,-0.5),(0,1,0.125)],
                                  den=[(1,0,1.0),(1,-1,-1.0)], l_ref=[0])

            elif name == 'r10':
                self.setup_ratios(name, num=[(0,-1,-0.125),(-1,0,0.5),(0,0,-0.75),(-1,1,0.5),(0,1,-0.125)],
                                  den=[(-1,1,1.0),(-1,0,-1.0)], l_ref=[1])

            elif name == 'd2nu':
                self.setup_ratios(name, num=[(0,1,1.0),(0,0,-2.0),(0,-1,1.0)])

            elif name == 'dnu':
                self.setup_ratios(name, num=[(0,0,1.0),(0,-1,-1.0)])

            elif name.startswith('dnu'):
                self.setup_ratios(name, num=[(0,0,1.0),(0,-1,-1.0)], l_ref=[int(name[3:])])

            elif name.startswith('avg_dnu'):
                try:
                    ls = [int(name[7:])]
                except ValueError:
                    ls = [l for l in np.unique(self.obs.modes['l']) if sum(self.obs.modes['l']==l) > 1]
                    if len(ls) == 0:
                        raise ValueError("no valid angular degrees for automatic <Δν> constraint")

                num_row = np.zeros(len(self.obs.modes))
                mat = np.zeros((len(ls)+1,len(ls)+1))

                for i, l in enumerate(ls):
                    L = self.obs.modes['l'] == l
                    n = self.obs.modes['n'][L]
                    freq = self.obs.modes['freq'][L]
                    sigma = self.sigma.modes['freq'][L]

                    mat[-1,-1] += np.sum(n**2/sigma**2)
                    mat[-1,i] = np.sum(n/sigma**2)
                    mat[i,-1] = mat[-1,i]
                    mat[i,i] = np.sum(1/sigma**2)

                invmat = np.linalg.inv(mat)

                for i, l in enumerate(ls):
                    L = self.obs.modes['l'] == l
                    n = self.obs.modes['n'][L]
                    sigma = self.sigma.modes['freq'][L]
                    num_row[L] = (invmat[-1,-1]*n + invmat[-1,i])/sigma**2

                self.extend_frequency_function([name], num_row)

        # non-zero elements in row of denominator matrix means it's a ratio
        # shorthand `is_ratio` makes code more readable
        # shouldn't be necessary
        self.freq_func_is_ratio = is_ratio = (self.freq_func_den != 0).any(axis=1)

        num_obs = self.freq_func_num.dot(self.obs.modes['freq'])
        den_obs = self.freq_func_den.dot(self.obs.modes['freq'])
        
        self.freq_func_obs = num_obs + 0
        self.freq_func_obs[is_ratio] /= den_obs[is_ratio]

        # 2d arrays for easy broadcasting when building Jacobian
        num_obs = num_obs[:,None]
        den_obs = den_obs[:,None]

        # compute the Jacobian matrix of frequency function
        J = self.freq_func_num*0
        J[~is_ratio] = self.freq_func_num[~is_ratio]
        if np.any(is_ratio):
            J[is_ratio] = self.freq_func_num[is_ratio]/den_obs[is_ratio] \
                - num_obs[is_ratio]/den_obs[is_ratio]**2*self.freq_func_den[is_ratio]

        # covariance matrix is J diag(σ²) J'
        J = J*self.sigma.modes['freq']
        self.freq_func_cov = J.dot(J.T)
        self.freq_func_icov = np.linalg.inv(self.freq_func_cov)


    def setup_ratios(self, name, num, den=[], l_ref=[0,1,2,3]):
        num_rows = []
        den_rows = []

        for mode in self.obs.modes:
            if not mode['l'] in l_ref:
                continue

            skip = False
            num_row = self.obs.modes['freq']*0
            for (delta_l, delta_n, coef) in num:
                for i, (l, n) in enumerate(zip(self.obs.modes['l'], self.obs.modes['n'])):
                    if mode['l'] + delta_l == l and mode['n'] + delta_n == n:
                        num_row[i] = coef
                        break
                else:
                    # finishing loop means we couldn't find a component of the numerator
                    skip = True
                    break

            # continue outermost loop
            if skip:
                continue

            den_row = self.obs.modes['freq']*0

            for (delta_l, delta_n, coef) in den:
                for i, (l, n) in enumerate(zip(self.obs.modes['l'], self.obs.modes['n'])):
                    if mode['l'] + delta_l == l and mode['n'] + delta_n == n:
                        den_row[i] = coef
                        break
                else:
                    # finishing loop means we couldn't find a component of the denominator
                    skip = True
                    break

            # continue outermost loop
            if skip:
                continue

            num_rows.append(num_row.copy())
            den_rows.append(den_row.copy())

        if len(num_rows) == len(den_rows):
            if len(num_rows) > 0:
                self.freq_func_num = np.vstack([self.freq_func_num, num_rows])
                self.freq_func_den = np.vstack([self.freq_func_den, den_rows])
                self.freq_func_names.extend(['%s_%i' % (name, i+1) for i in range(len(num_rows))])
        else:
            raise ValueError("something went wrong while setting up %s ratios" % name)


    def frequency_function(self, freqs):
        """Evaluate the frequency function using the frequencies in array
        ``freqs``, which must have already been selected to match the
        observations."""

        func = self.freq_func_num.dot(freqs)
        func[self.freq_func_is_ratio] /= self.freq_func_den[self.freq_func_is_ratio].dot(freqs)
        return func


    def set_chi2_weights(self, weight_option, seismic_weight_factor=1.0, nonseismic_weight_factor=1.0):
        """Assigns the relative weights for the log-likelihoods of the seismic
        and non-seismic parts (:math:`w_\\mathrm{seismic}` and
        :math:`w_\\mathrm{non-seismic}`) of the likelihood function
        depending on the number of terms in each.  e.g. Given
        :math:`T_\\mathrm{eff}`, [Fe/H] and four frequency separation
        ratios, we have two non-seismic terms and four seismic terms,
        i.e.  :math:`n_\\mathrm{non-seismic}=2` and
        :math:`n_\\mathrm{seismic}=4`.

        Parameters
        ----------
        weight_option: str

            For each option, the non-seismic and seismic weights are, respectively:

            * ``None`` or ``''``: both 1, ignoring the extra weight factors.

            * ``'absolute'``: both 1, multiplied by the extra weight factors.

            * ``'relative'``: 1 and :math:`n_\\mathrm{non-seismic}/n_\\mathrm{seismic}`

            * ``'reduced'``: both :math:`1/(n_\\mathrm{non-seismic}+n_\\mathrm{seismic}-1)`

            * ``'reduced_bis'``: :math:`1/(n_\\mathrm{non-seismic}-1)` and :math:`1/(n_\\mathrm{seismic}-1)`

        seismic_weight_factor: float
            Additional absolute weight by which to multiply the seismic component.
        nonseismic_weight_factor: float
            Additional absolute weight by which to multiply the non-seismic component.

        """

        n_seismic = len(self.freq_func_names)
        n_nonseismic = len(self.sigma.data.dtype.names)-1

        if weight_option is None:
            self.seismic_weight = self.nonseismic_weight = 1.0

        elif weight_option.lower() == 'absolute':
            self.seismic_weight = seismic_weight_factor
            self.nonseismic_weight = nonseismic_weight_factor

        elif weight_option.lower() == 'relative':
            self.seismic_weight = seismic_weight_factor*n_nonseismic/n_seismic
            self.nonseismic_weight = nonseismic_weight_factor

        elif weight_option.lower() == 'reduced':
            self.seismic_weight = seismic_weight_factor/(n_seismic+n_nonseismic-1)
            self.nonseismic_weight = nonseismic_weight_factor/(n_seismic+n_nonseismic-1)

        elif weight_option.lower() == 'reduced_bis':
            self.seismic_weight = seismic_weight_factor/(n_seismic-1)
            self.nonseismic_weight = nonseismic_weight_factor/(n_nonseismic-1)

        else:
            raise ValueError("unrecognized weight_option '%s'" % weight_option)


    def get_optimal_surface_correction(self, model, name=None, obs=None, sigma=None, fixed=None, maxiter=20):
        """Calculates the optimal surface correction parameters for ``model``
        relative to some observations.  By default, this uses the data
        stored in the Likelihood object (i.e. ``self``) but you can
        pass keyword arguments to override these with other values.

        Parameters
        ----------
        model: AIMS3 Model
            model for which to compute optimal surface correction parameters
        name: str, optional
            name of the surface correction, options are

            * ``None``: don't use any surface corrections

            * ``'Ball2014'``: use one-term (or *cubic*) correction by
              Ball & Gizon (2014)

            * ``'Ball2014_2'``: use two-term (or *combined*) correction
              by Ball & Gizon (2014)

            * ``'Kjeldsen2008'``: use power-law correction by Kjeldsen
              et al. (2008) with power ``fixed[0]`` and free amplitude

            * ``'Kjeldsen2008_scaling'``: like ``Kjeldsen2008`` but the
              power is based on the scaling relation by Sonoi et
              al. (2015), as implemented in :py:attr:`.b_Kjeldsen2008`

            * ``'Kjeldsen2008_2'`` or ``'PowerLaw'``: like
              ``Kjeldsen2008`` but both the amplitude and power are
              free

            * ``'Sonoi2015'``: use the modified Lorentzian by Sonoi et
              al. (2015) with power ``fixed[0]`` and free amplitude

            * ``'Sonoi2015_scaling'``: like ``Sonoi2015`` but the
              power is based on the scaling relation by Sonoi et
              al. (2015), as implemented in :py:attr:`.beta_Sonoi2015`

            * ``'Sonoi2015_2'``: like ``Sonoi2015`` but both the
              amplitude and power are free

        obs: array, optional
            observed frequencies
        sigma: array, optional
            uncertainties on observed frequencies
        fixed: iterable of floats
            fixed parameters for surface correction
        maxiter: int
            maximum number of iterations for Newton's method when
            optimising a surface correction with non-linear parameters

        Returns
        -------
        free: array
            optimal free parameters of the surface correction

        """
        if name is None:
            name = self.surface_correction_name
        if obs is None:
            obs = self.obs.modes['freq']
        if sigma is None:
            sigma = self.sigma.modes['freq']
        # each function should take mdl freqs, inertiae, obs and err
        # and return the function f and Jacobian J
        # then optimising linear and non-linear corrections both use Newton iterations by solving
        # JΔx = -f
        # for linear, use x₀ = 0 and do one iteration
        # for nonlinear, use smarter x₀ and iterate until convergence
        # need decent initial guesses for non-linear corrections
        # at some point refactor this by using a=0 and maxiter=1 for linear corrections
        
        # From AIMS_configure
        
        # e.g. power law
        # s(a,b) = a*freq**b
        #
        # f(a,b) = 0 = (obs-freq-s(a,b))/sigma
        # JΔx = -f
        #
        # J = -[df/da, df/db]/sigma

        # linear
        if name in ['Kjeldsen2008', 'Kjeldsen2008_scaling', 'Sonoi2015',
                    'Sonoi2015_scaling', 'Ball2014', 'Ball2014_2']:
            dfreq, J = model.get_surface_correction(name, [0,0], fixed=fixed)
            free = np.linalg.lstsq((J/sigma).T, (obs-model['freq'])/sigma, rcond=-1)[0]
        # nonlinear
        elif name in ['PowerLaw', 'Kjeldsen2008_2', 'Sonoi2015_2']:
            free = np.array([-2.,2.]) # TODO: using scaling corrections for initial guess
            for i in range(maxiter):
                dfreq, J = model.get_surface_correction(name, free)
                dfree = np.linalg.lstsq((J/sigma).T, (obs-model['freq']-dfreq)/sigma, rcond=-1)[0]
                free += dfree
                # TODO: if e.g. norm(dfree/free) < some tolerance, break
        else:
            raise ValueError("unrecognized surface_correction_name '%s'" % name)

        return free
    

    def load_observations(self, filename, comments=['#'], ignore_n=False):
        # format is:
        # * name, value, sigma (nonseismic)
        # * l, <n>, freq, sigma (seismic)
        # I'll assume nonseismic names are things in the grid
        data_dict = {}
        obs_modes = []
        sigma_modes = []

        with open(filename, 'r') as f:
            for line in f:
                for comment in comments:
                    line = line.split(comment)[0]
                words = line.split()
                if len(words) == 0:
                    continue
                try:
                    # if we can convert the first column to an int, it's a mode
                    l = int(words[0])
                    freq = float(words[-2])
                    sigma = float(words[-1])
                    if len(words) == 4 and not ignore_n:
                        n = int(words[1])
                    else:
                        n = unknown_n

                    obs_modes.append((l, n, freq, 0.0))
                    sigma_modes.append((l, n, sigma, 0.0))
                except ValueError:
                    # if not, it's either a nonseismic constraint or something is wrong
                    data_dict[words[0]] = (float(words[1]), float(words[2]))

        dtype = [('name', 'U64')] + [(k, float) for k in data_dict.keys()]
        obs_data = np.zeros((), dtype=dtype)
        sigma_data = np.zeros((), dtype=dtype)
        for k, (obs, sigma) in data_dict.items():
            obs_data[k] = obs
            sigma_data[k] = sigma

        obs_data['name'] = filename + '-obs'
        sigma_data['name'] = filename + '-sigma'

        self.obs = Model(obs_data, np.sort(np.array(obs_modes, dtype=mode_dtype), order=['l','n']))
        self.sigma = Model(sigma_data, np.sort(np.array(sigma_modes, dtype=mode_dtype), order=['l','n']))


    def save_observations(self, filename, float_fmt='%9g'):
        two_floats = ''.join([float_fmt, ' ', float_fmt, '\n'])

        with open(filename, 'w') as f:
            for k in self.sigma.data.dtype.names:
                 if k != 'name':
                     f.write(('%s ' + two_floats) % (k, self.obs.data[k], self.sigma.data[k]))

            for i, row in enumerate(self.obs.modes):
                if row['n'] == unknown_n:
                    f.write(('%i '+ two_floats) % (row['l'], row['freq'], self.sigma.modes[i]['freq']))
                else:
                    f.write(('%i %i ' + two_floats) % (row['l'], row['n'], row['freq'], self.sigma.modes[i]['freq']))

