from aims3.model import Model

import numpy as np
import numpy.lib.recfunctions as rf

class Track:
    """An object representing information about a sequence of
    :py:class:`.Model`\ s.  A ``Track`` would be a list of Models
    but the overhead of having an object for each model is large
    enough to make us store:

    * ``data``, an array of variables with one row per model;

    * ``modes``, a record array of all the modes, with column names
      ``'l'``, ``'n'``, ``'freq'`` and ``'inertia'``;

    * and ``idx``, an array of ``int``\ s to point to where each model
      starts and ends in ``modes``.

    You can access elements with ``[...]`` indexing in a number of ways.

    * An integer ``n`` returns the single *n*-th model in the track.

    * A slice or list of integers returns a new ``Track`` containing
      the corresponding models.

    * A string returns a list or array of the corresponding keys in
      each model.

    * A tuple of two ints ``(l,n)`` returns the frequency of the mode
      with the corresponding angular degree ``l`` and radial order
      ``n``, with ``NaN`` if the model doesn't have that mode.

    A by-product of the indexing is that you can iterate over the
    models in a track. i.e., ``for model in track: ...`` will work.

    A ``Track`` can be constructed by passing either a list of
    :py:class:`.Model`\ s or the arrays for the ``data``,
    ``modes`` and indices ``idx`` indicating the start and end of each
    model but you'll usually obtaining a track by loading a grid.

    """
    def __init__(self, *args):
        if len(args) == 1:
            models = args[0]
            data = np.hstack([model.data for model in models])
            modes = np.hstack([model.modes for model in models])
            idx = np.hstack([0, np.cumsum([len(model.modes) for model in models])])
        elif len(args) == 3:
            data, modes, idx = args
        else:
            raise TypeError

        self.data = data
        self.modes = modes
        self.idx = idx


    def __del__(self):
        # Daniel says we need to be aggressive about RAM
        del self.data
        del self.modes
        del self.idx


    def __len__(self):
        # number of Models
        return len(self.data)


    def __repr__(self):
        return 'aims3.track.Track(\ndata=%s,\nmodes=%s,\nidx=%s\n)' % (self.data, self.modes, self.idx)


    def __getitem__(self, key):
        if isinstance(key, (int, np.integer)):
            if key < 0:
                key = len(self) + key

            return Model(self.data[key], self.modes[self.idx[key]:self.idx[key+1]])

        elif isinstance(key, slice):
            # let NumPy resolve the slice; after which we just have to handle
            # the index properly
            idx_dn = self.idx[:-1][key]
            idx_up = self.idx[1:][key]
            idx = np.hstack([0, np.cumsum(idx_up-idx_dn)])
            modes = np.hstack([self.modes[idx_dn[i]:idx_up[i]] for i in range(len(idx_dn))])
            return Track(self.data[key].copy(), modes, idx)

        elif isinstance(key, tuple):
            freq = np.full(len(self), np.nan)
            for i in range(len(self)):
                try:
                    modes = self.modes[self.idx[i]:self.idx[i+1]]
                    freq[i] = modes[(modes['l']==key[0])&(modes['n']==key[1])]['freq']
                except ValueError:
                    continue

            return freq

        elif isinstance(key, list):
            if all(isinstance(i, (int, np.integer)) for i in key):
                return Track([self[i] for i in key])
            elif all(isinstance(k, (str, tuple)) for k in key):
                return [self[k] for k in key]
            else:
                raise KeyError("keys in a list must all be str or int, not mixed or %s" % type(key[0]))

        elif key in self.data.dtype.names:
            return self.data[key]

        elif key == 'rho':
            return self.data['M']/self.data['R']**3

        elif key == 'numax':
            return self.data['M']/self.data['R']**2/np.sqrt(self.data['Teff']/5772.)*3078.0

        elif key == 'nuac':
            return self.data['M']/self.data['R']**2/np.sqrt(self.data['Teff']/5772.)*5000.0

        else:
            raise KeyError("invalid key '%s' for aims3.track.Track" % key)


    def __setitem__(self, key, value):
        if key in self.data.dtype.names:
            self.data[key] = value
        else:
            self.data = rf.merge_arrays([self.data, np.array(value, dtype=[(key, float)])], flatten=True)


    def append(self, model):
        """Extend the track by the single given :py:class:`.Model`."""
        self.data = np.hstack([self.data, model.data])
        self.modes = np.hstack([self.modes, model.modes])
        self.idx = np.hstack([self.idx, self.idx[-1]+len(model.modes)])


    def extend(self, models):
        """Extend the track by the given :py:class:`.Track` or list of
        :py:class:`.Model`\ s."""
        for model in models:
            self.append(model)


    def interpolate(self, t, age_name='t', weight_tol=1e-8):
        """Interpolates a model linearly along the track at the given value of
        the age parameter.

        Parameters
        ----------
        t: float
            the value of the age parameter at which to interpolate
        age_name: str
            the column name of the the age parameter (default='t')
        weight_tol: float
            if either weight in the linear interpolation is less than
            ``weight_tol``, treat it as zero

        Returns
        -------
        model: :py:class:`.Model`
            the model interpolated at ``t``

        """
        if t < self[age_name].min():
            raise ValueError("%s is less than track's minimum" % age_name)
        if t > self[age_name].max():
            raise ValueError("%s is greater than track's maximum" % age_name)

        dt = np.diff(self[age_name])

        if np.all(dt > 0):
            i = np.where(self[age_name] < t)[0][-1]
        elif np.all(dt < 0):
            i = np.where(t < self[age_name])[0][-1]
        else:
            raise ValueError("Column %s is not strictly monotonic" % age_name)

        model1, model2 = self[i], self[i+1]
        weight = (t - model1[age_name])/(model2[age_name] - model1[age_name])
        assert(0 <= weight and weight <= 1)
        if weight <= weight_tol:
            return model1
        elif weight >= 1-weight_tol:
            return model2
        else:
            return model1*(1-weight) + model2*weight
