from aims3.track import Track

import pickle
import numpy as np
import yaml
from scipy.spatial import Delaunay

# only what's necessary
default_metadata = {'prefix': '',
                    'postfix': '',
                    # not convinced you actually need these
                    # 'G': 6.67430e-8,
                    # 'Msun': 1.9884098706980504e+33,
                    # 'Lsun': 3.828e33,
                    # 'Rsun': 6.957e10,
                    # 'Z_X_sun': 0.02293,
                    }

def load_text_grid(filename, mode_loader=lambda z: np.genfromtxt(z, names=True, dtype=None, encoding='utf-8'),
                   progress=False):
    """Load a grid from a listing in ``filename``.

    Parameters
    ----------
    filename: str
        filename of the main grid listing
    mode_loader: function
        a function that takes a mode table filename and returns a
        structured NumPy array with columns ``'l'``, ``'n'``,
        ``'freq'`` and ``'inertia'``.
    progress: bool
        show a basic progress report

    Returns
    -------
    grid: :py:class:`.Grid`
    """


    def vprint(*args, **kwargs):
        if progress:
            print(*args, **kwargs)

    with open(filename, 'r') as f:
        lines = []
        for line in f:
            lines.append(line)
            if lines[-1].startswith('---'):
                break
        else:
            raise ValueError("couldn't find --- to indicate end of metadata")

        metadata = {**default_metadata, **yaml.safe_load('\n'.join(lines[:-1]))}
        param_names = metadata.pop('param_names')

        data = np.genfromtxt(f, names=True, dtype=None, encoding='utf-8')

    tracks = []
    current_params = np.array([data[0][k] for k in param_names])
    start = 0

    fmt = '%%%ii of %i models loaded...' % (len('%s' % len(data)), len(data))
    vprint('', end='')
    
    for i, row in enumerate(data):
        vprint('\r' + fmt % i, end='', flush=True)
        params = np.array([row[k] for k in param_names])

        # if params are the same, it's the same track: append and update index
        if not np.all(params == current_params):
            modes = [mode_loader(metadata['prefix'] + data[j]['name'] + metadata['postfix'])
                     for j in range(start, i)]
            idx = np.hstack([0, np.cumsum([len(m) for m in modes])])
            tracks.append(Track(data[start:i], np.hstack(modes), idx))

            start = i
            current_params = params

    # do last track
    modes = [mode_loader(metadata['prefix'] + data[j]['name'] + metadata['postfix'])
             for j in range(start, len(data))]
    idx = np.hstack([0, np.cumsum([len(m) for m in modes])])
    tracks.append(Track(data[start:], np.hstack(modes), idx))

    vprint('\r ' + fmt % len(data) + ' Done.')

    return Grid(tracks=tracks, param_names=param_names, metadata=metadata)


def load_binary_grid(filename):
    """Unpickle a precomputed binary grid from ``filename``.

    Parameters
    ----------
    filename: str
        filename of the pickled ``Grid``.

    Returns
    -------
    grid: :py:class:`.Grid`
    """
    with open(filename, 'rb') as f:
        tracks, param_names, metadata, distortion_matrix, tessellation = pickle.load(f)

    return Grid(tracks=tracks, param_names=param_names, metadata=metadata,
                distortion_matrix=distortion_matrix, tessellation=None)


class Grid:
    """An object representing information about a grid of stellar tracks.
    A ``Grid`` is really a list of :py:class:`.Track`\ s with a
    few extra functions for linear interpolation at parameters between
    the tracks, a dictionary of arbitrary metadata, and a list of
    column names that specify the track parameters (e.g. ``['M',
    'Z0']``).

    You can access elements with ``[...]`` indexing in a number of ways.

    * An integer ``n`` returns the *n*-th track in the grid.

    * A slice or list of integers returns a new ``Grid`` containing
      the corresponding models.

    * A string or list of strings either returns the corresponding
      entry from the metadata dict or an array with the corresponding
      grid parameters.  Anything else will raise a ``KeyError``.

    """
    def __init__(self, tracks=[], param_names=[], metadata={},
                 distortion_matrix=None, tessellation=None):
        self.param_names = param_names
        self.tracks = tracks
        self.metadata = {**default_metadata, **metadata}
        # self.metadata['Teffsun'] = (self.metadata['Lsun']/
        #                             (4.*np.pi*5.67037441918442945397e-5*self.metadata['Rsun']**2))**0.25
        self.tessellation = tessellation

        self.refresh_params()
        if distortion_matrix is None:
            self.distortion_matrix = np.eye(len(param_names))
        else:
            self.distortion_matrix = distortion_matrix


    def __del__(self):
        del self.param_names
        del self.tracks
        del self.metadata
        del self.distortion_matrix
        del self.tessellation


    def __len__(self):
        return len(self.tracks)


    def __str__(self):
        return '\n'.join([
            'param_names: [%s]' % ', '.join(self.param_names),
            'param_min: %s' % np.min(self.params, axis=0),
            'param_max: %s' % np.max(self.params, axis=0),
            'tracks: %i' % len(self),
            '', '————————', 'metadata', '————————',
            yaml.dump(self.metadata),
        ])


    def __getitem__(self, key):
        if isinstance(key, (int, np.integer)):
            return self.tracks[key]
        elif isinstance(key, str):
            if key in self.param_names:
                return self.params[:,self.param_names.index(key)]
            elif key in self.metadata.keys():
                return self.metadata[key]
            else:
                raise KeyError("str key '%s' not in param_names or metadata" % key)
        elif isinstance(key, slice):
            return Grid(tracks=self.tracks[key], param_names=self.param_names,
                        metadata=self.metadata)
        elif isinstance(key, list):
            if all(isinstance(i, (int, np.integer)) for i in key):
                return Grid(tracks=[self.tracks[i] for i in key],
                            param_names=self.param_names,
                            metadata=self.metadata)
            elif all(isinstance(k, str) for k in key):
                return [self[k] for k in key]
            else:
                raise KeyError("keys in a list must all be str or int, not mixed or %s" % type(key[0]))
        else:
            raise KeyError("key '%s' of type %s not a valid key" % (key, type(key)))


    def refresh_params(self):
        """If we touch the list of tracks, we need to update the list of parameter values."""
        self.params = np.array([[track[name][0] for name in self.param_names]
                                     for track in self.tracks])


    def append(self, track):
        """Append the :py:class:`.Track` object ``track`` to the grid."""
        self.tracks.append(track)
        self.refresh_params()


    def extend(self, tracks):
        """Extend the grid by the given ``Grid`` or list of :py:class:`.Track`\ s."""
        for track in tracks:
            self.tracks.append(track)

        self.refresh_params()


    def tessellate(self, perturb=True):
        """Compute the tessellation.  This needs to be called before
        :py:meth:`interpolate`.  If ``perturb=True`` (the default),
        fractional perturbations on the order of 1e-11 are added to
        the points so that the tessellation of a regular grid is
        robust to transformations."""
        self.refresh_params() # defensive, probably unnecessary, but probably not that slow

        if perturb:
            # lo = self.params.min(axis=0)
            # hi = self.params.max(axis=0)
            # N = np.array([len(np.unique(row)) for row in self.params.T])
            # delta = 1e-8*np.max(np.abs(self.params))*np.sin((self.params-lo)/(hi-lo)*(N-1)*np.pi/2
            #                                                  +np.arange(len(lo))*np.pi)
            state = np.random.get_state()
            np.random.seed(19827) # needs to be reproducible
            delta = 1e-11*np.max(np.abs(self.params))*(2*np.random.rand(*self.params.shape)-1)
            np.random.set_state(state)
        else:
            delta = 0

        self.tessellation = Delaunay(np.dot(self.params + delta, self.distortion_matrix))


    def interpolate(self, params, age_name='t', scale=False, weight_tol=1e-8):
        """Interpolate a stellar model at the give parameters.  You must call
        :py:meth:`tessellate` before calling `interpolate`.

        Parameters
        ----------
        params: iterable of floats
            The parameters at which to interpolate.  The last
            parameter is the evolutionary parameter (usually age); the
            others are the grid parameters in the same order as the
            attribute ``param_names``.
        age_name: str
            Column name of the age parameter (usually the age;
            default='t').
        scale: bool
            If ``True``, interpolate at a scaled version of the
            evolutionary parameter.
        weight_tol: float
            If any weight in the linear interpolation is less than
            ``weight_tol``, treat it as zero.

        Returns
        -------
        model: :py:class:`.Model`
            The model interpolated at the parameters ``params``.

        """
        # TODO: implement distortion matrix
        # distort this if distortion_matrix is not None
        p = np.dot(params[:-1], self.distortion_matrix)
        t = params[-1]

        s = self.tessellation.find_simplex(p)
        if s == -1:
            raise ValueError('parameters are outside grid')

        # process to get "barycentric" co-ordinates. i.e. coefficients
        # from AIMS, which closely follows SciPy docs
        # https://docs.scipy.org/doc/scipy/reference/generated/scipy.spatial.Delaunay.html
        T = self.tessellation.transform[s]
        b = T[:-1].dot(p-T[-1])
        weights = np.r_[b, 1-b.sum()]
        assert(np.all((-weight_tol <= weights) & (weights <= 1+weight_tol)))
        tracks = [self.tracks[i] for weight, i in zip(weights, self.tessellation.simplices[s])
                  if weight > weight_tol]
        weights = weights[weights > weight_tol]
        # TODO: restore sum of weights to 1

        # if scale, get the age as a fraction of weighted start and end
        # TODO: catch t_min and t_max
        # TODO: test that scaling still works with decreasing age parameters
        if scale:
            t_min, t_max = np.sum([(weight*track[age_name].min(), weight*track[age_name].max())
                                   for track, weight in zip(tracks, weights)], axis=0)
            eta = (t-t_min)/(t_max-t_min)
            models = [track.interpolate((1-eta)*track[age_name][0] + eta*track[age_name][-1],
                                        age_name=age_name)
                      for track in tracks]
       # otherwise just interpolate to that age
        else:
            models = [track.interpolate(t, age_name=age_name) for track in tracks]

        # we can't loop over zip(models, weights) because we need to modify the models in place
        for i, weight in enumerate(weights):
            # models[i].modes['freq'] *= np.sqrt(models[i].data['R']**3/models[i].data['M'])
            models[i] *= weight
            
        model = sum(models[1:], start=models[0])
        # model.modes['freq'] *= np.sqrt(model.data['M']/model.data['R']**3)
        # should check for other consistency issues here too. e.g. L~R²Teff⁴, R~(M/R³)
        # actually, this all needs to be handled in __add__, not here
        model.data['name'] = 'Grid.interpolate'
        return model


    def to_binary_file(self, filename, **kwargs):
        """Save the grid to ``filename`` in binary format using
        ``pickle.dump``.  Extra keyword arguments ``kwargs`` are
        passed to ``pickle.dump``.

        This is the inverse of :py:func:`load_binary_grid`.
        """
        with open(filename, 'wb') as f:
            pickle.dump((self.tracks, self.param_names, self.metadata,
                         self.distortion_matrix, self.tessellation), f,
                        **kwargs)


    def to_text_file(self, filename, save_models=False):
        """Save the grid listing to ``filename`` in plain text format.
        If ``save_models`` is ``True``, also save the model files.

        This is the inverse of :py:func:`load_text_grid`.
        """
        with open(filename, 'wt') as f:
            f.write(yaml.dump(self.metadata))
            f.write('param_names: [' + ', '.join(self.param_names) + ']\n')
            f.write('---\n')
            names = self.tracks[0].data.dtype.names
            np.savetxt(f, np.hstack([track.data for track in self.tracks]),
                       header=' '.join(names), fmt=' '.join(['%s']*len(names)))
            
        if save_models:
            for track in self.tracks:
                for model in track:
                    model_filename = self.metadata['prefix'] + model['name'] + self.metadata['postfix']
                    np.savetxt(model_filename, model.modes, fmt=' %2i %6i %23.16e %23.16e',
                               header='%1s %6s %23s %23s' % ('l', 'n', 'freq', 'inertia'))
