import numpy as np
import numpy.lib.recfunctions as rf

mode_dtype = [('l', np.uint8), ('n', np.int16),
              ('freq', np.float64), ('inertia', np.float64)]

def get_mode_loader(mode_format):
    """Get a mode loader for :py:meth:`.Grid.load_text_grid` that reads
    files in one of AIMS's standard formats (see :ref:`realdata` for
    details).

    Parameters
    ----------
    mode_format: str
        case-insensitive insensitive format identifier; one of

        * ``'simple'`` or ``'cles'``

        * ``'cles_mode'``

        * ``'plato'``

        * ``'gyre'``

        * ``'agsm'``

    Returns
    -------
    mode_loader: function(str)
        a function that takes a filename as an argument and returns a
        structured NumPy array with columns ``'l'``, ``'n'``,
        ``'freq'`` and ``'inertia'``, preferably using the dtype
        :py:attr:`aims3.model.mode_dtype`

    """

    if mode_format.lower() in ['simple', 'cles']:
        # l, n, freq (uHz), inertia = [0, 1, 2, 4]
        # skip one line of header
        def mode_loader(filename):
            return np.genfromtxt(filename, usecols=[0, 1, 2, 4],
                                 dtype=mode_dtype, skip_header=1)
    elif mode_format.lower() == 'cles_mod':
        # l, n, freq (Hz) = [0, 1, 3] (no inertia)
        # arbitrary comments starting with #
        def mode_loader(filename):
            modes = np.genfromtxt(
                filename, usecols=[0, 1, 3, 2], dtype=mode_dtype, comments='#')
            modes['freq'] *= 1e6
            modes['inertia'] *= np.nan
            return modes
    elif mode_format.lower() == 'plato':
        # one-liner in AIMS, which therefore defines format
        def mode_loader(filename):
            return np.loadtxt(filename, usecols=[0, 1, 2, 3], dtype=mode_dtype)
    elif mode_format.lower() == 'gyre':
        def mode_loader(filename):
            data = np.genfromtxt(filename, names=True, dtype=None, encoding='utf-8',
                                 skip_header=5)
            return data[['l','n_pg','Refreq','E_norm']].astype(mode_dtype)
    elif mode_format.lower() == 'agsm':
        def mode_loader(filename):
            css = []

            with open(filename, 'rb') as f:
                while True:
                    if not f.read(4):
                        break
                    else:
                        css.append(np.fromfile(f, dtype=float, count=50))
                        f.read(4)

            css = np.array(css)
            modes = np.zeros(len(css), dtype=mode_dtype)
            modes['l'] = css[:,17]
            modes['n'] = css[:,18]
            modes['freq'] = css[:,36]*1e3
            modes['inertia'] = css[:,23]
            return modes

    return mode_loader


class Model:
    """An object representing information about a single stellar model,
    currently comprising a single row record array ``data`` and a mode
    array ``modes``.

    Mode data should be sorted by (l,n)
    (e.g. (0,10),(0,11),(1,10),...) but the constructor doesn't
    enforce this.

    You can add models together or multiply them by a constant, as a
    convenience for the linear interpolation routines.  These
    operations are defined so that the frequencies are interpolated as
    dimensionless frequencies, so the behaviour is possibly not
    well-defined (and not tested) for weights outside the interval
    (0, 1].

    You can also access elements with ``[...]`` indexing in a number of
    ways.

    * An integer, slice, or list of integers returns the corresponding
      rows of the mode data.

    * A string returns either corresponding single non-seismic data
      value (e.g. ``['R']`` for radius) or that column of the mode
      data (e.g. ``['l']`` returns the angular degrees).  

    * A tuple of two ints ``(l,n)`` returns the mode with the
      corresponding angular degree ``l`` and radial order ``n``.

    * If not part of the data, ``rho`` returns the mean density in
      solar units by computing ``M/R**3`` and ``logg`` returns the
      surface gravity assuming a solar value of 4.438.

    * If not part of the data, ``numax`` or ``nuac`` can be
      :math:`\\nu_\mathrm{max}` (in μHz) or :math:`\\nu_\mathrm{ac}`
      (i.e. the acoustic cutoff frequency, in μHz), both derived from
      scaling relations.

    """
    # we seem to sometimes need this to avoid type errors when doing
    # arithmetic with models
    __array_priority__ = -999999
    
    def __init__(self, data, modes):
        # mode data is a recarray
        # TODO: separate name from data
        # looks like mode data has to be a recarray for now
        # TODO: if you have an array of a single type, try coercing with list(zip(*modes.T))
        self.data = data
        self.modes = modes.astype(mode_dtype)
        # self.modes = np.sort(modes.astype(mode_dtype), order=['l','n'])


    def __del__(self):
        # Daniel says we need to be aggressive about RAM
        del self.data
        del self.modes


    def __getitem__(self, key):
        if isinstance(key, (int, np.integer)) or key in self.modes.dtype.names:
            return self.modes[key]
        elif isinstance(key, tuple):
            return self.modes['freq'][(self.modes['l']==key[0])&(self.modes['n']==key[1])]
        elif isinstance(key, slice):
            return Model(self.data.copy(), self.modes[key].copy())
        elif isinstance(key, list):
            if all(isinstance(i, (int, np.integer)) for i in key):
                return Model(self.data.copy(), self.modes[[i for i in key]].copy())
            elif all(isinstance(k, (str, tuple)) for k in key):
                return [self[k] for k in key]
            else:
                raise KeyError("keys in a list must all be str, int or (int,int), not mixed or %s" % type(key[0]))
        elif key in self.data.dtype.names:
            return self.data[key]
        elif key == 'logg':
            # fallback for when it isn't on the grid
            return 4.438 + np.log10(self.data['M']/self.data['R']**2)
        elif key == 'rho':
            return self.data['M']/self.data['R']**3
        elif key == 'numax':
            return self.data['M']/self.data['R']**2/np.sqrt(self.data['Teff']/5772.)*3078.0
        elif key == 'nuac':
            return self.data['M']/self.data['R']**2/np.sqrt(self.data['Teff']/5772.)*5000.0
        else:
            raise KeyError("invalid key '%s' for aims3.model.Model" % key)


    def __setitem__(self, key, value):
        if key in self.data.dtype.names:
            self.data[key] = value
        else:
            self.data = rf.merge_arrays([self.data, np.array(value, dtype=[(key, float)])], flatten=True)[0]


    def __len__(self):
        return len(self.modes)


    def __repr__(self):
        return 'aims3.model.Model(\ndata=%s,\nmodes=%s\n)' % (self.data, self.modes)


    # TODO: stop using recarray for Model.data and Track.data
    # these are probably slow: adding takes ~0.5ms, multiplying by a constant about ~0.05ms
    def __add__(self, other):
        if isinstance(other, Model):
            dtype = [('name','U8')] + [(k,'float') for k in self.data.dtype.names
                                       if k in other.data.dtype.names and k != 'name']
            data = np.zeros((), dtype=dtype)

            # TODO: consistency of L via R²Teff⁴ in Model.__add__
            
            for k in data.dtype.names:
                if k == 'name':
                    data[k] = '__add__'
                elif k == 'R':
                #     data[k] = (data[k]/fac_self + other.data[k]/fac_other)
                    continue
                elif k in other.data.dtype.names:
                    data[k] = self.data[k] + other.data[k]
                else:
                    raise KeyError("'%s' is not an column in Model.data" % k)

            # can only do this after M has been done
            if 'R' in data.dtype.names:
                data['R'] = ((self.data['M']+other.data['M'])/(self['rho']+other['rho']))**(1/3)

            # if 'Teff' in data.dtype.names:
            #     data['Teff'] = (self.data['Teff']/self.data['L']**0.25*self.data['R']**0.5
            #                     + other.data['Teff']/other.data['L']**0.25*other.data['R']**0.5) \
            #                     /data['R']**0.5*data['L']**0.25
                
            I_self, I_other = self.intersect_modes(other)
            modes = self.modes[I_self].copy()
            # modes['freq'] += other.modes['freq'][I_other]
            modes['freq'] = (modes['freq']/np.sqrt(self['rho'])
                             + other.modes['freq'][I_other]/np.sqrt(other['rho'])) \
                             * np.sqrt(data['M']/data['R']**3)
            modes['inertia'] += other.modes['inertia'][I_other]
                
            return Model(data, modes)
        else:
            return NotImplemented


    def __radd__(self, other):
        return self.__add__(other)


    def __mul__(self, other):
        if isinstance(other, (int, np.integer, float, np.floating)):
            data = self.data.copy()
            for k in data.dtype.names:
                if k == 'name':
                    data[k] = '__mul__'
                elif k == 'R':
                    # data[k] *= np.sign(other)
                    continue
                # elif k == 'Teff':
                #     data[k] *= np.abs(other)**1.25
                else:
                    data[k] *= other

            modes = self.modes.copy()
            modes['freq'] *= other*np.sqrt(np.abs(other))
            modes['inertia'] *= other
            return Model(data, modes)
        else:
            return NotImplemented


    def __rmul__(self, other):
        return  self.__mul__(other)

    # TODO: implement imul and iadd to save some time by avoiding object creation

    def __rtruediv__(self, other):
        if isinstance(other, (int, np.integer, float, np.floating)):
            data = self.data.copy()
            for k in data.dtype.names:
                if k == 'name':
                    data[k] = '__rdiv__'
                else:
                    data[k] = other/data[k]

            modes = self.modes.copy()
            modes['freq'] = other/modes['freq']
            modes['inertia'] = other/modes['inertia']
            return Model(data, modes)
        else:
            return NotImplemented        


    def __sub__(self, other):
        return self.__add__(-1 * other)


    def __rsub__(self, other):
        return (-1 * self).__add__(other)


    def copy(self):
        """Return a copy of the model to avoid aliasing errors."""
        return self*1


    def intersect_modes(self, other, assume_unique=True):
        """Get the indices of the modes that occur both in this model
        (i.e. ``self``) and some other object ``other`` that has an
        attribute ``modes`` with integer columns ``l`` and ``n``.
        This includes other ``Model`` objects but can also be abused
        to compute the intersection with other objects.

        Parameters
        ----------
        other: an AIMS3 Model
            the model for which to return matching modes
        assume_unique: bool
            if ``True`` (the default), assume there are no modes with
            the same ``n`` and ``l``.

        Returns
        -------
        I, J: two arrays of ints
            the indices of the matching modes in ``self`` and ``other``

        """
        # this is a cheap trick to get something that orders by n and l
        # if there's ever a model with |n| >= 10000, this will break
        return np.intersect1d(10000*self.modes['l']+self.modes['n'],
                              10000*other.modes['l']+other.modes['n'],
                              return_indices=True, assume_unique=assume_unique)[1:]
                              

    def set_n(self, ref=None):
        # can't use Model arithmetic before n is set
        # ref is a Model; if given, use that (~AIMS.assign_n)
        # otherwise guess using Dnu (~AIMS.guess_n)
        if ref is None:
            radial = self.modes[self.modes['l'] == 0]
            d_Dnu = np.diff(radial['freq'])

            # initial guess is median of differences that are less than 1.5x smallest difference
            # this won't work if there isn't at least one pair of consecutive radial modes
            Dnu = np.nanmedian(d_Dnu[d_Dnu < 1.5*np.nanmin(d_Dnu)])
            eps = np.nanmedian(radial['freq']/Dnu-radial['freq']//Dnu)
            radial['n'] = np.round(radial['freq']/Dnu-eps)-1

            # refine by fitting a straight line to (n,freq) and assign
            Dnu, eps = np.polyfit(radial['n'], radial['freq'], 1)

            self.modes['n'] = np.round((self.modes['freq']-eps)/Dnu-self.modes['l']/2)
        else:
            for l in np.unique(self.modes['l']):
                D = self.modes['freq'][self.modes['l']==l][:,None]-ref.modes['freq'][ref.modes['l']==l]
                while np.any(np.isfinite(D)):
                    # find indices of pair with closest values
                    i, j = np.unravel_index(np.argmin(D**2), D.shape)

                    # have to use np.where so that we modify values in place
                    self.modes['n'][np.where(self.modes['l']==l)[0][i]] = \
                        ref.modes['n'][np.where(ref.modes['l']==l)[0][j]]

                    # mark that row and column as matched so that we don't match them again
                    D[i,:] = D[:,j] = np.inf


    # _2 means both parameters free
    # _scaling means exponents from scaling relations in Sonoi et al. (2015)
    # otherwise amplitude is free and power fixed by configuration
    def get_surface_correction(self, name, free, fixed=None):
        """Compute the surface correction of the kind named by ``name`` using
        free or fixed parameters in ``free`` or ``fixed``.  Also
        returns the Jacobian matrix of the frequency difference with
        respect to the parameters so that the parameter values can be
        efficiently optimised.

        Parameters
        ----------
        name: str
            name of the surface correction to use, one of

            * ``''``: no correction
            * ``'Ball2014'``: the one-term (or *cubic*) correction by Ball & Gizon (2014)
            * ``'Ball2014_2'``: the two-term (or *combined*) correction by Ball & Gizon (2014)
            * ``'Kjeldsen2008'``
            * ``'Kjeldsen2008_scaling'``
            * ``'Kjeldsen2008_2'``
            * ``'Sonoi2015'``
            * ``'Sonoi2015_scaling'``
            * ``'Sonoi2015_2'``
        free: iterable of floats
            the free parameters for the named surface correction
        fixed: iterable of floats
            the fixed parameter for the named surface correction

        Returns
        -------
        dfreq: array of floats
            the surface correction, which should be added to the raw
            model frequencies
        J: 2-d array of floats
            the Jacobian matrix of the frequency differences with
            respect to the free parameters

        """

        freq = self.modes['freq']/self['numax']

        if name == '':
            dfreq = self.modes['freq']*0
            J = np.zeros((0, len(self.modes)))

        elif name == 'Ball2014':
            J = (freq**3/self.modes['inertia'])[None,:]
            dfreq = free[0]*J[0]

        elif name == 'Ball2014_2':
            J = np.vstack([freq**3, 1/freq])/self.modes['inertia']
            dfreq = np.dot(free, J)

        elif name == 'Sonoi2015':
            J = (1-1/(1+freq**fixed))[None,:]
            dfreq = free[0]*J[0]

        elif name == 'Sonoi2015_scaling':
            J = (1-1/(1+freq**self.beta_Sonoi2015))[None,:]
            dfreq = free[0]*J[0]

        elif name == 'Sonoi2015_2':
            powfreqb = freq**free[1]
            J = np.vstack([1-1/(1+powfreqb),
                           free[0]*freq**free[1]*np.log(freq)/(1+freq**free[1])**2])
            dfreq = free[0]*(1-1/(1+powfreqb))

        elif name == 'Kjeldsen2008':
            J = (freq**fixed)[None,:]
            dfreq = free[0]*J[0]

        elif name == 'Kjeldsen2008_scaling':
            J = (freq**self.b_Kjeldsen2008)[None,:]
            dfreq = free[0]*J[0]

        elif name in ['PowerLaw', 'Kjeldsen2008_2']:
            powfreqb = freq**free[1]
            dfreq = free[0]*powfreqb
            J = np.vstack([powfreqb, dfreq*np.log(freq)])

        else:
            raise ValueError("unrecognized surface correction name '%s'" % name)

        return dfreq, J


    @property
    def b_Kjeldsen2008(self):
        """
        Return the exponent for the Kjeldsen et al. (2008) surface correction
        based on the scaling relation by Sonoi et al. (2015).
        """
        return 10.0**(-3.16*np.log10(self['Teff']) + 0.184*self['logg'] + 11.7)

 
    @property
    def beta_Sonoi2015(self):
        """
        Return the exponent for the Sonoi et al. (2015) surface correction
        based on the scaling relation by Sonoi et al. (2015).
        """
        return 10.0**(-3.86*np.log10(self['Teff']) + 0.235*self['logg'] + 14.2)
