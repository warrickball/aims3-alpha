Command-line interface
======================

AIMS3 includes a command-line program ``aims3``, which re-implements
many of the functions of the operations available in AIMS's
``AIMS.py`` script.  For fine-tuned control over your model-fitting, I
suggest you construct your own scripts using AIMS3 as a library.

Grid manipulation
+++++++++++++++++

Interpolation tests
+++++++++++++++++++

MCMC analysis
+++++++++++++
