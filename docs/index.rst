.. AIMS3 documentation master file, created by
   sphinx-quickstart on Thu Apr  8 13:19:08 2021.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

AIMS3
=====

AIMS3 is a re-implementation of Asteroseismic Inference on a Massive
Scale (AIMS), a pipeline for fitting pre-computed grids of stellar
models to seismic and non-seismic observational constraints.  The
long-term goal is to re-implement most of AIMS's original
functionality.  This development version includes libraries for much
of the core functionality but not yet the original pipeline.

If you use AIMS, you're welcome to try AIMS3 and let me know how it
goes.

A rudimentary introduction to the code is given in the user guide.
The API documentation describes the interfaces to the classes and
functions.

Why re-implement AIMS?
++++++++++++++++++++++

At the end of 2020, we started working on AIMS with the objective that
some of its components could be used in the analysis pipeline for
PLATO.  We identified a number of issues and set out some objectives
for which it was not clear how we could iteratively improve the
existing code to the point we wanted it to be.  Instead, I decided to
solve many of these issues at once by starting over, guided by the
experience of working with AIMS.

Installation
++++++++++++

::

    git clone https://gitlab.com/warrickball/aims3-alpha.git
    cd aims3-alpha
    python setup.py develop <--user>


.. toctree::
   :maxdepth: 1
   :caption: User guide

   intro
   grids
   posterior
   sampling
   cli

.. toctree::
   :maxdepth: 1
   :caption: Module APIs

   api/stats
   api/grid
   api/track
   api/model
   api/tdc

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
