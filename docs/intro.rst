Introduction
============

Fitting stellar models to observations
++++++++++++++++++++++++++++++++++++++

Suppose you've observed some isolated star (i.e. not a binary or
multiple) in the sky.  Perhaps you've recorded its brightness in a
number of passbands.  Perhaps you've recorded a spectrum at optical
wavelengths.  How old is this star?  One way to estimate the star's
age is to compare these observations to a collection of models
computed using a stellar evolution code, which solves a set of
equations, derived with some physical assumptions and approximations.

For single stars, however, there are generally more free parameters
than we can constrain using our observations.  The degeneracy is
usually solved by making strict assumptions about the parameters.
e.g. convection's length-scale is the same as whatever gives us a good
fit to the Sun, for which we *do* have enough information to constrain
all the free parameters. [#]_ Even then, age estimates in particular
can be quite uncertain.

The situation changes for solar-like oscillators.  With high-quality
data, one can at measure the frequencies and identify the angular
degrees of multiple individual modes, which leaves us with more
observables than unknowns.  Even if we can't reliably detect
individual mode frequencies, we can often detect the characteristic
spacing between modes of the same angular degree (the large
separation) and the frequency at which the modes have the greatest
power.

AIMS3 is a Python library that provides components for pipelines to
fit pre-computed stellar models to observations that includes data
from oscillations (*seismic* data) and other sources, like optical
spectra (*nonseismic* data).  The principal components of this process
are:

1. some sort of function that takes stellar model parameters
   (e.g. mass and age) and returns predicted observables,
2. a function that defines how well a given set of predicted (or
   theoretical) observables matches actual observations, and
3. a function that combines these functions to estimate the true
   distribution of the model parameters.

Following the original AIMS implementation, the core methods that
AIMS3 uses for each of these steps are:

1. linear interpolation in a pre-computed grid of stellar models,
2. a configurable posterior function, and
3. Markov Chain Monte Carlo (MCMC) sampling.

The rest of the user guide describes the concepts behined each of
these components.  For the details of the interfaces, see the API
documentation.

.. [#] Unless you start adding more free parameters, like for
       convective overshooting.
