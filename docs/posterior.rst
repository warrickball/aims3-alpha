Likelihoods, Priors and Posteriors
==================================

AIMS3 provides a ``Likelihood`` class for bookkeeping.  User or
pipeline scripts provides priors with which to construct a posterior
distribution function.

.. _freqfunc:

The frequency function
++++++++++++++++++++++

The simplest way of using data for individual mode frequencies is to
them directly but, for many reasons, we may wish to instead fit using
some functions of the frequency.  These may include averaged
properties like the average large separation, fit by linear regression
of the radial modes against their radial order, or
frequency-insensitive quantities like separation ratios.

AIMS3's ``Likelihood`` object uses a *frequency function* :math:`f(\nu)`,
which maps the individual frequencies :math:`\nu` onto the quantities
we'll actually fit.  *f* is therefore a vector-valued function of the
vector *ν*.  AIMS3 is restricted to a mixture of functions that are
either linear in *ν* (i.e. f(ν)=Aν for some matrix A) or a simple
ratio of linear maps (i.e. f(ν)=Aν/Bν for matrices A and B).  If we
just want to use the mode frequencies directly, then we'd use a linear
map *A=I*.

To propagate the uncertainties on the mode frequencies, which are
defined by the covariance matrix *C*, we need the Jacobian matrix *J*,
in which case the covariance of the frequency function is :math:`C' =
JCJ^\mathrm{T}`.  For a linear map, *J=A*.  For a ratio of linear
maps, the quotient rule gives

.. math::

   J = \frac{1}{B\nu}A-\frac{A\nu}{(B\nu)^2}B

AIMS3 constructs the frequency function and its Jacobian by stacking
matrices for a sequence of options.  The current options, given a set
of mode frequencies :math:`\nu_{l,n}`, are

* ``nu<l>``: use the individual frequencies

* ``nu_min<i>``: use the *i*-th lowest radial mode frequency

* ``r02``: use the separation ratios

  .. math::

     r_{02,n} = \frac{\nu_{0,n}-\nu_{2,n-1}}{\nu_{1,n}-\nu_{1,n-1}}
     
* ``r13``: use the separation ratios

  .. math::

     r_{13,n} = \frac{\nu_{1,n}-\nu_{3,n-1}}{\nu_{0,n+1}-\nu_{0,n}}
* ``r01``: use the separation ratios

  .. math::

     r_{01,n}=\frac{\nu_{0,n-1}-4\nu_{1,n-1}+6\nu_{0,n}-4\nu_{1,n}+\nu_{0,n+1}}{8(\nu_{1,n}-\nu_{1,n-1})}
     
* ``r10``: use the separation ratios

  .. math::

     r_{10,n}=-\frac{\nu_{1,n-1}-4\nu_{0,n}+6\nu_{1,n}-4\nu_{0,n+1}+\nu_{1,n+1}}{8(\nu_{0,n+1}-\nu_{0,n})}

* ``d2nu``: use the second frequency differences

  .. math::

     \Delta_2\nu_{n,l} = \nu_{l,n-1}-2\nu_{l,n}+\nu_{l,n+1}

* ``dnu<l>``: use the mode-by-mode frequency differences :math:`\Delta\nu_{l,n}=\nu_{l,n}-\nu_{l,n-1}`
* ``avg_dnu<l>``: use the average large separation :math:`\langle\Delta\nu_l\rangle` (found by linear regression)

Where there is an optional angular degree ``<l>``, it can be omitted
to use all available modes. e.g., ``nu1`` will add all the dipole mode
frequencies and ``avg_dnu2`` will add the average large separation of
the quadrupole modes.

If a combination of options leads to an uninvertible covariance matrix
*C'*, the AIMS3 will raise a ``LinAlgError``.

Surface corrections
+++++++++++++++++++

Functional forms:

* ``Ball2014``

  .. math::

     \delta\nu = a_3\left(\frac{\nu}{\nu_\mathrm{max}}\right)^3\mathcal{I}^{-1}

* ``Ball2014_2``

  .. math::

     \delta\nu = \left(a_{-1}\left(\frac{\nu}{\nu_\mathrm{max}}\right)^{-1}
     +a_3\left(\frac{\nu}{\nu_\mathrm{max}}\right)^3\right)\mathcal{I}^{-1}

* ``Kjeldsen2008`` or ``PowerLaw``

  .. math::

     \delta\nu = a\left(\frac{\nu}{\nu_\mathrm{max}}\right)^b

* ``Sonoi2015``

  .. math::

     \delta\nu = a\left(1-\frac{1}{1+\left(\frac{\nu}{\nu_\mathrm{max}}\right)^b}\right)

     
Module options:

* ``Ball2014``: the one-term (or *cubic*) correction by Ball & Gizon (2014).  The coefficient is free.
* ``Ball2014_2``: the two-term (or *combined*) correction by Ball & Gizon (2014).  Both coefficients are free.
* ``Kjeldsen2008``: The linear coefficient *a* is free; the power-law index *b* is fixed.
* ``Kjeldsen2008_scaling``: The linear coefficient *a* is free; the
  power-law index *b* is determined by the scaling relation by Sonoi et
  al. (2015).
* ``Kjeldsen2008_2``: Both *a* and *b* are free.
* ``PowerLaw``: equivalent to ``Kjeldsen2008_2``.
* ``Sonoi2015``: The linear coefficient *a* is free; the power-law index *b* is fixed.
* ``Sonoi2015_scaling``: The linear coefficient *a* is free; the
  power-law index *b* is determined by the scaling relation by Sonoi et
  al. (2015).
* ``Sonoi2015_2`` or ``PowerLaw``: Both *a* and *b* are free.


Observational data format
+++++++++++++++++++++++++

The standard file format for observations is a mixture of comments,
blank lines, lines for observed mode frequencies and lines for
observed non-seismic data (e.g. effective temperature).  These can be
mixed in any order.  The following is an example of a valid file:

::

   # comments are OK
   0 21 2866.79 0.50
   M_H 0.09 0.09
   
   0 3001.18 0.30
   0 3135.44 0.50 # comments can follow a row of data, too
   Teff 5761 84
   
Lines starting with an integer are presumed to be data for modes.  If
there are four numbers, the second is presumed to be the radial order,
though it is ignored if the argument ``ignore_n=True`` is passed.  If
there is no radial order (or it is ignored), the data is given a null
value defined by :py:const:`.stats.unknown_n`.  The second last and
last numbers are the frequency and its uncertainty.

Lines starting with a string are treated as a non-seismic constraint
and added to the ``Likelihood`` object with that string as a key and
the two floats as the mean and uncertainty.

The values and uncertainties need only be in the same units as each
other and the same units as the grid, unless you're getting very
advanced in manipulating the grid or observations.


