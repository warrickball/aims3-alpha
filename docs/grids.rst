Models, Tracks & Grids
======================

Let's first define some outputs of stellar evolution and pulsation
codes.

**model**
    The structure of a star at a given point in time, usually
    represented by some fundamental variables (e.g. pressure,
    temperature) on a discrete mesh, and from which all other
    quantities, observable (e.g. effective temperature) or not
    (e.g. central pressure), are derived.

**track**
    A sequence of stellar models, usually in increasing order of age
    (an *evolutionary* track), starting from an evolution code's
    initial model until the evolution's (usually user-defined)
    endpoint.

**grid**
    A collection of tracks, created by varying some parameters of a
    track that specify the initial state (e.g. mass) or anything else
    that might be constant over a star's life (e.g. mixing length
    parameter).

AIMS3's :py:class:`.Model`, :py:class:`.Track` and :py:class:`.Grid`
objects reflect this hierarchy.

Interpolation
+++++++++++++

Test data
+++++++++

For testing, AIMS3 includes a :py:mod:`.test_data_creator` module
that uses some very approximate relationships to create very crude
stellar models that have the correct structure for AIMS3 to work.
These are mainly used for testing but you can use these to experiment
with what AIMS3 data looks like before worrying about creating a grid.

.. _realdata:

Real data
+++++++++

In order for AIMS3 to work with a grid of real models, the data needs
to be prepared in a simple but specific format that AIMS3 can read
with the :py:func:`.grid.load_text_grid` function.  The two main
components are a *listing*, which contains global data for each model
as well as the filenames of mode data for each model.  The format of
the mode data doesn't really matter as long as you can provide a
function that takes a filename and returns a structured NumPy array
with columns

* ``'l'``
* ``'n'``
* ``'freq'`` and
* ``'inertia'``.

A valid main listing will start with something like this

::

   # The first block is YAML, for which # indicates a comment,
   # so this will be ignored
   prefix: /path/to/modes/
   postfix: .freq
   param_names: [M, Z0]
   ---
   #         name       M      Z0      t      Zs     Xs    Teff       R       L
   M0.99_Z0.02_t0    0.99    0.02    0.0    0.02    0.7    5607    0.90    0.72
   M0.99_Z0.02_t1    0.99    0.02    1.0    0.02    0.7    5636    0.91    0.75
   M0.99_Z0.02_t2    0.99    0.02    2.0    0.02    0.7    5663    0.92    0.78
   ...

The main listing starts with a YAML block of metadata.  The only
required part of this metadata is a list of column names for the
parameters that are distinguish each track in the grid. e.g. in this
case the initial mass and initial metallicity, indicated by the
columns ``M`` and ``Z0``.  The metadata is terminated by a standard
YAML document separator, ``---``.

The rest of the file is the table of data for each star in the grid,
with one row per model.  The first column must be the partial filename
containing the modes for that model, such that ``prefix + name +
postfix`` is the complete filename. e.g. in this case, the mode data
for the first model is in ``/path/to/modes/M0.99_Z0.02_t0.freq``.  The
remaining columns are various floats that can appear in any order but
must at least contain ``M`` and ``R``.  For certain other quantities
derived from scaling relations (notably :math:`\nu_\mathrm{max}` or
:math:`\nu_\mathrm{ac}` if not in the table) then one of ``Teff`` or
``L`` must be present too.  These parameters should be in solar units,
which can be provided by the metadata keys ``Msun``, ``Rsun``,
etc. but IAU B3 values will be used in their absence and they don't
matter for the interpolation routines.

There is technically no requirement for the format of the mode data
files.  Instead,  :py:func:`.load_text_grid` takes a ``mode_loader``
argument, which is a function that takes a filename and returns
a structured NumPy array with the following columns:

* ``'l'``, the angular degree;

* ``'n'``, the radial order;

* ``'freq'``, the mode frequency (in the same units as the
  observations); and

* ``'inertia'``, the mode inertia.

For example, the default ``mode_loader`` is

::

   np.genfromtxt(filename, names=True, dtype=None, encoding='utf-8')

which implies a mode data file of a form like

::

    # l      n    freq     inertia
      0     20    3369    9.54e-10
      0     21    3527    8.27e-10
      0     22    3684    7.39e-10
      ...

For convenience, AIMS3 provides the function
:py:func:`get_mode_loader`, which takes a (case-insensitive) string
defining a file format and returns a ``mode_loader`` for the following
formats that were implemented in AIMS.

``'simple'`` and ``'cles'``
~~~~~~~~~~~~~~~~~~~~~~~~~~~

This format is similar to the default, except that it has an extra
column between the ``'freq'`` and ``'inertia'`` columns.  Though the
example has the column names in the first line, the line is actually
skipped entirely so can contain any information.  Frequencies are in μHz.

::

    # l      n    freq     unused    inertia
      0     20    3369        0.0   9.54e-10   a fixed number of columns is read,
      0     21    3527        0.0   8.27e-10   so you can put whatever you want here
      0     22    3684        0.0   7.39e-10
      ...


``'cles_mod'``
~~~~~~~~~~~~~~

Very similar to the ``simple``/\ ``cles`` format but the unused column
is the third and there is no inertia.  All the inertia are set to
``np.nan``.  Lines starting with ``#`` are ignored as comments and the
first line is no longer automatically ignored.  Frequencies are in Hz.

::

    # l      n     unused        freq
      0     20        0.0    0.003369   a fixed number of columns is read,
      0     21        0.0    0.003527   so you can put whatever you want here
    # you can also indicate comments like this
      0     22        0.0    0.003684
      ...

``'plato'``
~~~~~~~~~~~

A simple format, with the four columns in order and comments indicated
by ``#``.  Frequencies in μHz. e.g.

::

    # l      n    freq    inertia   column names are nice but this could be anything
      0     20    3369   9.54e-10
      0     21    3527   8.27e-10   a fixed number of columns is read,
      0     22    3684   7.39e-10   so you can put whatever you want here
      ...

``'gyre'``
~~~~~~~~~~

A GYRE text summary.  To work, GYRE's ``summary_item_list`` namelist
controls must include ``'l'``, ``'n_pg'``, ``'freq'`` and
``'E_norm'``.

``'agsm'``
~~~~~~~~~~

An ADIPLS grand summary (usually ending or starting with ``agsm``),
which is a binary format produced by ADIPLS.
